from nodes.AST import *

__author__ = "Argenti Marco, Aulecchia Alessio, Azara Antonio"


class UnrecognizedNodeError(PyCoException):
    """ Raised when creating a node for an unsupported operation. """

    def __init__(self, node, line):
        self.node = node
        self.line = line

    def __repr__(self):
        return "Object {} cannot be flattened!".format(repr(self.node))


class StrayReturnStatementError(PyCoException):
    """ Raised when a return statement is outside of a function. """

    def __init__(self, line):
        self.line = line

    def __repr__(self):
        return "Return statement outside of function body."


class Flattener():

    function_depth = 0

    to_wrap = {Identifier, Integer, Boolean, GetEnvVar, NoneRef, String}

    @staticmethod
    def idents(names, line):
        """Return a list of identifiers with the given names."""

        return [Identifier(id_name, line) for id_name in names]

    @staticmethod
    def assigns(targets, operations):
        """Return a list of assignments of the operations into the targets."""

        return [Assign([t], op, op.line) for t, op in zip(targets, operations)]

    @staticmethod
    def labels(total, line):
        """Return a list of <total> new labels."""

        return [Label(line) for i in range(total)]

    def flatten_unary(self, node, name):
        operator, line = node.operator, node.line
        expr, operation = self.idents([self.fresh_name()] + [name], node.line)

        stmts = self.flatten(node.expr, expr.name)
        stmts += [Assign([operation], Unary(operator, expr, line), line)]

        return stmts

    def flatten_boolean_binary(self, node, name):
        operator, line = node.operator, node.line
        operation, left, right = self.idents([name] + self.fresh_names(2), line)

        stmts = self.flatten(node.left, left.name)
        stmts += self.flatten(node.right, right.name)

        binary = BooleanBinary(operator, left, right, line)
        stmts += [Assign([operation], binary, line)]

        return stmts

    def flatten_binary(self, node, name):
        operator, line = node.operator, node.line
        operation, left, right = self.idents([name] + self.fresh_names(2), line)

        stmts = self.flatten(node.left, left.name)
        stmts += self.flatten(node.right, right.name)

        binary = Binary(operator, left, right, line)
        stmts += [Assign([operation], binary, line)]

        return stmts

    def wrap(self, node, name):
        """ Flatten the node by simply wrapping it in an assignment."""

        return [Assign([Identifier(name, node.line)], node, node.line)]

    def flatten_assign(self, node, name):
        stmts = self.flatten(node.expr, name)

        # Get list of targets (len times the fresh_name) and create ids
        names = (name,) * len(node.targets)
        identifiers = self.idents(names, node.line)

        # Add assign statements
        stmts.extend(self.assigns(node.targets, identifiers))
        return stmts

    def flatten_augmented_assign(self, node, name):
        operator, line = node.operator.strip("="), node.line

        # Transform augmented assignment in binary operation and flatten it
        operation = Binary(operator, node.target, node.expr, line)
        stmts = self.flatten_binary(operation, name)

        stmts += [Assign([node.target], Identifier(name, node.line), line)]

        return stmts

    def flatten_if(self, node, name):
        line = node.line

        ids = self.idents(self.fresh_names(3), node.line)
        condition, cond_cast, cond_untagged = ids

        if_label, else_label, after_label = self.labels(3, node.line)

        stmts = self.flatten(node.cond, condition.name)
        stmts += [Assign([cond_untagged], Untag(condition, line), line)]
        stmts += [Assign([cond_cast], Cast1(cond_untagged, line), line)]

        # Perform the correct branch depending on whether there is an else
        if node.else_body:
            stmts += [If(cond_cast, [if_label], [else_label], node.line)]
        else:
            stmts += [If(cond_cast, [if_label], [after_label], node.line)]

        # Add if body
        stmts += [if_label]
        for statement in node.body:
            stmts += self.flatten(statement, self.fresh_name())
        stmts += [Goto(after_label, node.line)]

        # Add else body (if it exists)
        if node.else_body:
            stmts += [else_label]
            for statement in node.else_body:
                stmts += self.flatten(statement, self.fresh_name())
            stmts += [Goto(after_label, node.line)]

        stmts += [after_label]

        return stmts

    def flatten_while(self, node, name):
        line = node.line

        ids = self.idents(self.fresh_names(3), node.line)
        condition, cond_cast, cond_untagged = ids

        cond_label, body_label, after_label = self.labels(3, line)

        # Add evaluation of condition and check
        stmts = [Goto(cond_label, line), cond_label]
        stmts += self.flatten(node.cond, condition.name)
        stmts += [Assign([cond_untagged], Untag(condition, line), line)]
        stmts += [Assign([cond_cast], Cast1(cond_untagged, line), line)]
        stmts += [If(cond_cast, [body_label], [after_label], line)]

        # Ad body of while
        stmts.append(body_label)
        for statement in node.body:
            stmts += self.flatten(statement, self.fresh_name())

        # Add goto for check and after label
        stmts += [Goto(cond_label, line), after_label]

        return stmts

    def flatten_print(self, node, name):
        if len(node.args) > 1:
            ids = self.idents(self.fresh_names(len(node.args)), node.line)

            stmts = []
            for index in range(len(node.args)):
                stmts += self.flatten(node.args[index], ids[index].name)
                stmts += [Print([ids[index]], False, line=node.line)]

            # Print new line
            #stmts.append(Print([], node.line))

        else:
            stmts = self.flatten(node.args[0], name)
            nl = node.newline
            stmts += [Print([Identifier(name, node.line)], nl, line=node.line)]

        return stmts

    def flatten_input(self, node, name):
        line = node.line

        statements = []

        # Add print for prompt if any
        if len(node.args) > 0:
            temp_identifier = Identifier(self.fresh_name(), node.line)
            statements += self.flatten(node.args[0], temp_identifier.name)
            statements += [Print([temp_identifier], False, True, node.line)]

        statements += [Assign([Identifier(name, line)], Input(None, line))]

        return statements

    def flatten_lambda_container(self, container, name):
        assign = self.flatten(container.function)
        return assign + self.flatten(container.call, name)

    def flatten_function_call(self, call, name):
        line = call.line
        stmts = []

        arg_ids = self.idents(self.fresh_names(len(call.args)), line)
        for index in range(len(call.args)):
            stmts += self.flatten(call.args[index], arg_ids[index].name)

        ids = self.idents(self.fresh_names(5), line)
        function, untagged_fun, function_ptr, env, env_ptr = ids

        # Put closure in a register (easier to compile)
        stmts += self.flatten(call.name, function.name)
        stmts += [Assign([untagged_fun], Untag(function, NoneRef, line), line)]

        # Type for pointer cast
        fun_type = "i64 ({})**".format(", ".join((len(arg_ids) + 1) * ["i64"]))

        # Cast int to actual function pointer
        pointer_cast = IntToPointer(untagged_fun, fun_type, line)
        stmts += [Assign([function_ptr], pointer_cast, line)]

        # Dispatch call on new pointer
        call.name = function_ptr
        call.args = [untagged_fun] + arg_ids

        stmts += [Assign([Identifier(name, line)], call, line)]
        return stmts

    def flatten_method_call(self, method, name):
        stmts = []
        _vars = []

        caller_fname = self.fresh_name()
        stmts += self.flatten(method.caller, caller_fname)

        for arg in method.args:
            arg_fname = self.fresh_name()
            stmts += self.flatten(arg, arg_fname)
            _vars.append(arg_fname)

        ret_type = None
        if method.name.name in {"pop", "count", "index", "get", "slice",
                                "slice_up_to", "slice_from", "length"}:
            ret_type = "int"

        if method.name.name in List.specific_functions:
            method.name.name = "list_" + method.name.name

        if method.name.name == "list_pop" and len(method.args) == 0:
            method.name.name += "_last"

        _vars.insert(0, Identifier(caller_fname, method.line))
        method_node = RuntimeCall(method.name, _vars, ret_type, method.line)

        if ret_type:
            stmts += [Assign([Identifier(name)], method_node)]
        else:
            stmts += [method_node]

        return stmts

    def flatten_class(self, node, name):
        pass

    def flatten_function(self, node, name):
        self.function_depth += 1
        body = []
        for statement in node.code:
            body += self.flatten(statement)

        # Make sure there is always a return statement
        if not isinstance(body[len(body) - 1], Return):
            ret_id = Identifier(name, node.line)
            body += [Assign([ret_id], NoneRef(node.line), node.line)]
            body += [Return(ret_id, node.line)]

        self.function_depth -= 1
        return [Function(node.name, node.args, body, node.line)]

    def flatten_return(self, node, name):
        """Flatten a return statement."""

        if self.function_depth <= 0:
            raise StrayReturnStatementError(node.line)

        fr_name = self.fresh_name()
        val = self.flatten(node.value, fr_name)

        return val + [Return(Identifier(fr_name), node.line)]

    def flatten_make_closure(self, node, name):
        line = node.line
        ids = self.idents([name, self.fresh_name(), node.old_name], line)
        closure, untagged_c, old_name = ids

        # Get closure pointer and untag it
        stmts = [Assign([closure], node, node.line)]
        stmts += [Assign([old_name], closure, line)]
        stmts += [Assign([untagged_c], Untag(closure, NoneRef, line), line)]

        # Flatten make closure into separate make closure calls
        for var in node.env.vars:
            temp = Identifier(self.fresh_name(), node.line)
            stmts += [Assign([temp], var, node.line)]

            # Put variable to correct closure
            env = MakeEnv(node.line)
            env.closure = untagged_c
            env.vars = [temp]
            if isinstance(var, Identifier):
                env.key = var.name
            else:
                env.key = var.var.name
            stmts += [env]

        return stmts

    def flatten_discard(self, node, name):
        return self.flatten(node.expr, name)

    def flatten_program(self, node, name):
        """Flatten a Program node."""

        functions = []
        statements = []

        for function in node.functions:
            functions.extend(self.flatten(function))

        for statement in node.statements:
            statements.extend(self.flatten(statement))

        return Program(statements, functions, node.literals, line=0)

    def flatten_list(self, node, name):
        """Flatten a list."""

        elems = []
        stmts = []

        for elem in node.elems:
            var = Flattener.fresh_name()
            stmts += self.flatten(elem, var)
            elems.append(Identifier(var, node.line))

        lst = List([], node.line)
        stmts += [Assign([Identifier(name)], lst, node.line)]

        for ident in elems:
            args = [Identifier(name, node.line), ident]
            stmts += [RuntimeCall("list_append", args, False, node.line)]

        return stmts

    @staticmethod
    def fresh_names(tot=1):
        """Return the name for a new temporary variable.

        Return a name for a temporary variable as <dot>t<number>. The dot at the
        beginning ensures that there are no variables with the same name (in
        python variables cannot start with dots) and will allow us to recognize
        registers in the compiler.
        """

        if not hasattr(Flattener.fresh_names, 'last'):
            Flattener.fresh_names.last = 0

        names = []
        for i in range(tot):
            names += [".t{}".format(Flattener.fresh_names.last + i)]
        Flattener.fresh_names.last += tot

        return names

    @staticmethod
    def fresh_name():
        return Flattener.fresh_names()[0]

    @staticmethod
    def fresh_var_name():
        """Return a fresh variable name. """

        if not hasattr(Flattener.fresh_var_name, 'last'):
            Flattener.fresh_var_name.last = 0

        Flattener.fresh_var_name.last += 1
        return "$id{}".format(Flattener.fresh_var_name.last - 1)

    @staticmethod
    def fresh_name_function():
        #make a name for lambda function
        if not hasattr(Flattener.fresh_name_function, 'last'):
            Flattener.fresh_name_function.last = 0

        Flattener.fresh_name_function.last += 1
        return "$lambda{}".format(Flattener.fresh_name_function.last - 1)

    def flatten(self, node, name=None):
        """Interprets a node from an AST and returns its result.
        If the node cannot be recognized, then an exception is raised.
        """
        if not name:
            name = self.fresh_name()

        if node.__class__ in self.to_wrap:
            return self.wrap(node, name)

        method_name = "flatten_" + node.to_snake_case()
        if hasattr(self, method_name):
            return getattr(self, method_name)(node, name)
        else:
            raise UnrecognizedNodeError(node, node.line)


def flatten_program(program):
    flattener = Flattener()
    return flattener.flatten(program)



