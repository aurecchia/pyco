#!/usr/bin/env python


import nodes.AST as Ast
from os import path
from inspect import getfile, currentframe
from nodes.AST import PyCoException

__author__ = "Argenti Marco, Aurecchia Alessio, Azara Antonio"


class NotCompilableError(PyCoException):
    """ Raised when creating a node for an unsupported operation. """

    def __init__(self, operation, line):
        self.op = operation
        self.line = line

    def __repr__(self):
        return "[!] Node {} cannot be compiled directly!".format(repr(self.op))


class UnallocatedVariableError(PyCoException):
    """Raised when an uninitialized variable is used."""

    def __init__(self, variable, line):
        self.variable = variable
        self.line = line

    def __repr__(self):
        return "\n[!] Using an unallocated variable {}!".format(self.variable)


class NotAProgramError(PyCoException):
    """Raised when trying to compile something which is not an AST.Program."""

    def __init__(self, obj):
        self.obj = obj

    def __repr__(self):
        return "\n[!] Compile takes an AST.Program, got {}!".format(self.obj)


class InvalidInstruction(PyCoException):
    """Exception during the creations of unsupported operations."""

    def __init__(self, instruction):
        self.inst = instruction

    def __repr__(self):
        return "[!] Instruction {} is not supported!".format(repr(self.inst))


class Compiler():

    allocated_variables = set()
    free_vars_names = set()

    @staticmethod
    def allocate_vars(identifiers):
        """Allocate all variables in the given program.

            Allocate all the variables in the program if they are actual variables:
            registers are skipped since they need not allocation.

            The function also keep track of all allocated variables, so that use of
            an unallocated on can be detected.
            """

        allocation_format = "%{} = alloca i64, align 8"

        scope = set()
        program = []
        for node in identifiers:
            if isinstance(node, Ast.Assign):
                ident = node.targets[0]
            elif isinstance(node, Ast.Identifier):
                ident = node
            else:
                continue

            # Only allocate unallocated variables
            if not ident.is_register() and ident.name not in scope:
                scope.add(ident.name)

                program.append(allocation_format.format(ident.name))

        Compiler.allocated_variables.update(scope)

        return program

    def compile_boolean_binary(self, binary):
        left = binary.left
        right = binary.right

        op = binary.operator;

        return "", "call i64 @{}(i64 %{}, i64 %{})".format(op, left, right)

    def compile_binary(self, binary):
        left = binary.left
        right = binary.right

        instructions = {"+": "add",
                        "-": "sub",
                        "*": "mul",
                        "/": "intdiv",
                        "//": "floordiv",
                        "%": "mod",
                        "<<": "left_shift",
                        ">>": "right_shift",
                        "&": "bitand",
                        "|": "bitor",
                        "^": "bitxor",
                        "<": "lt",
                        ">": "gt",
                        "<=": "lte",
                        ">=": "gte",
                        "==": "eq",
                        "!=": "neq"}
        op = instructions[binary.operator]

        return "", "call i64 @{}(i64 %{}, i64 %{})".format(op, left, right)

    def compile_compatible_operation(self, node):

        before_left, left = self.compile_node(node.left)
        before_right, right = self.compile_node(node.right)

        before = before_left + before_right
        return before, "call i64 @are_compatible(i64 {}, i64 {})".format(left, right)

    def compile_unary(self, unary):
        instructions = {"+": "uadd",
                        "-": "usub",
                        "~": "bitnot",
                        "not": "not"}

        before, expr = self.compile_node(unary.expr)
        op = instructions[unary.operator]

        return "", "call i64 @{}(i64 {})".format(op, expr)

    def compile_valid_operation(self, node):
        before, expr = self.compile_node(node.operand)
        return before, "call i64 @valid_operation(i64 {})".format(expr)

    def compile_assign(self, assign):
        target, expr = assign.targets[0], assign.expr

        if target.is_register():
            if isinstance(expr, Ast.Identifier):
                # Assigning from register

                if assign.expr.is_register():
                    # Passing value from register to register
                    inst = "{} = add i64 {}, 0"
                else:
                    # Loading from variable to register
                    inst = "{} = load i64* {}, align 8"
            else:
                # Assigning from an expression (always goes to register)
                inst = "{} = {}"

            before_target, target = self.compile_node(target)
            before_expr, expr = self.compile_node(expr)
            before = before_target + before_expr
            return before, inst.format(target, expr)


        before_target, target = self.compile_node(target)
        before_expr, expr = self.compile_node(expr)
        before = before_target + before_expr
        # Assigning to variable (always comes from register)
        return before, "\n\tstore i64 {}, i64* {}, align 8".format(expr, target)

    def compile_identifier(self, ident):
        if not ident.is_register() and ident.name not in \
                Compiler.allocated_variables:
            raise UnallocatedVariableError(ident.name, ident.line)

        return "", "%{}".format(ident.name)

    def compile_integer(self, integer):
        """ Wrap integer with call to tag function. """

        return "", "call i64 @tag(i64 {}, i32 0)".format(integer.value)

    def compile_boolean(self, boolean):
        """ Wrap boolean with call to tag function. """

        return "", "call i64 @tag(i64 {}, i32 1)".format(boolean.value)

    def compile_string(self, str):

        length = len(str.content) - 1
        cast = "bitcast ([{} x i8]* @{} to i8*)".format(length, str.reference)
        return "", "call i64 @string_new(i8* {})".format(cast)

    def compile_none_ref(self, none):
        """ Compile the non reference. """

        return "", "call i64 @tag(i64 0, i32 3)"

    def compile_list(self, node):
        return "", "call i64 @list_new_default()"

    def compile_if(self, node):
        cond = node.cond

        # Labels are the first item in a flattened body
        then_l = node.body[0].name
        else_l = node.else_body[0].name

        branch_format = "br i1 %{}, label %{}, label %{}"

        return "", branch_format.format(cond, then_l, else_l)

    def compile_goto(self, goto):
            return "", "br label %{}".format(goto.label.name)

    def compile_label(self, label):
        return "", "{}:".format(label.name)

    def compile_function(self, function):
        # Get comma separated arguments with type
        args = ["i64 %.env"]
        for arg in function.args:
            compiled_arg = self.compile_node(arg)[1]
            args += ["i64 {}".format(compiled_arg)]
        args_list = ", ".join(args)

        # Get header for function (return type, name and arguments)
        header = "define i64 @{}({})".format(function.name, args_list)

        # Code to allocate all variables + code of body
        alloc = self.allocate_vars(function.code)

        for arg in function.args:
            r_name = arg.name[1:]
            name = arg.name
            alloc += ["%{} = alloca i64, align 8".format(r_name)]

            alloc += ["store i64 %{}, i64* %{}, align 8".format(name, r_name)]
            self.allocated_variables.add(r_name)
        alloc.append("")

        body = []
        for statement in function.code:
            before, compiled = self.compile_node(statement)
            body += [before + compiled]

        # Return code for function
        return header + " {\n" + "\n\t".join(alloc + body) + "\n}\n"

    def compile_int_to_pointer(self, get):
        ptr_format = "inttoptr i64 {} to {}"
        before, closure = self.compile_node(get.closure_reg)
        return before, ptr_format.format(closure, get.dest_type)

    def compile_function_call(self, call):
        """ Creates the call for a function. """
        env = ["i8* {}".format(call.args[0])]

        before = []
        before_env, env = self.compile_node(call.args[0])
        before += [before_env]

        args = ["i64 {}".format(env)]
        for arg in call.args[1:]:
            before_arg, compiled_arg = self.compile_node(arg)
            before += [before_arg]
            args += ["i64 {}".format(compiled_arg)]

        args = ", ".join(args)

        arg_types = ["i64"] + ["i64"] * (len(call.args) - 1)
        args_string = "{}".format(", ".join(arg_types))

        load_name = self.fresh_name()
        load_format = "%{} = load i64 ({})** {}, align 8"
        load = load_format.format(load_name, args_string, self.compile_node(
            call.name)[1])

        return load + "\n\t", "call i64 %{}({})".format(load_name, args)

    def compile_runtime_call(self, call):
        typed = map(lambda n: "i64 %" + str(n), call.args)
        args = ", ".join(typed)
        ret = "i64" if call.ret else "void"
        return "", "call {} @{}({})".format(ret, call.name, args)

    def compile_print(self, print_statement):
        """ Create call to print function. """

        if len(print_statement.args) == 0:
            return "", "call void @print_empty()"
        before, arg = self.compile_node(print_statement.args[0])
        if print_statement.prompt:
            return "", "call void @prompt(i64 {})".format(arg)
        if print_statement.newline:
            return "", "call void @print_nl(i64 {})".format(arg)
        else:
            return "", "call void @print(i64 {})".format(arg)

    def compile_input(self, input_function):
        """ create call to input function. """
        return "", "call i64 @input()"

    def compile_make_closure(self, make_closure):
        name = make_closure.function_name
        args = ", ".join(["i64"] + make_closure.argc * ["i64"])

        cast = "i8* bitcast (i64 ({})* @{} to i8*)".format(args, name)

        return "", "call i64 @make_closure({})".format(cast)

    def compile_make_env(self, env):
        closure = "i64 {}".format(self.compile_node(env.closure)[1])
        key_f = "i8* getelementptr inbounds ([{} x i8]* @.var_{}, i32 0, i32 0)"
        key = key_f.format(len(env.key) + 1, env.key)
        val = "i64 {}".format(self.compile_node(env.vars[0])[1])

        return "", "call void @put_env({}, {}, {})".format(closure, key, val)

    def compile_get_env_var(self, get_var):
        self.free_vars_names.add(get_var.var)
        ptr_format = "i8* getelementptr inbounds ([{} x i8]* @{}, i32 0, i32 0)"
        var_name = get_var.var.name
        string = ptr_format.format(len(var_name) + 1, ".var_" + var_name)

        return "", "call i64 @get_env(i64 %.env, {})".format(string)

    def compile_tag(self, tag):
        """ Create call to tag function. """

        before, expr = self.compile_node(tag.expr)
        return before, "call i64 @tag(i64 {}, i64 {})".format(expr, tag.type_code)

    def compile_safe_tag(self, tag):
        """ Create call to checked tag function. """

        before_expr, expr = self.compile_node(tag.expr)
        before_left, left = self.compile_node(tag.left)
        before_right, right = self.compile_node(tag.right)
        before = before_expr + before_left + before_right
        call = "call i64 @c_tag(i64 {}, i64 {}, i64 {})"
        return before, call.format(expr, left, right)

    def compile_untag(self, untag):
        """ Create call to untag function. """

        before, expr = self.compile_node(untag.expr)
        return before, "call i64 @untag(i64 {})".format(expr)

    def compile_cast32(self, cast):
        """ Create call to cast32 function. """

        before, expr = self.compile_node(cast.value)
        return before, "zext i1 {} to i64".format(expr)

    def compile_cast1(self, cast):
        """ Create call to cast1 function. """

        before, expr = self.compile_node(cast.value)
        return before, "icmp ne i64 {}, 0".format(expr)

    def compile_cast(self, cast):
        before, expr = self.compile_node(cast.value)
        return before, "bitcast i64* {} to %{}".format(expr, cast.type)

    def compile_return(self, ret):
        before, expr = self.compile_node(ret.value)
        return before, "ret i64 {}".format(expr)

    @staticmethod
    def fresh_name(tot=1):
        """Return the name for a new temporary variable.

            Return a name for a temporary variable as <dot>t<number>. The dot at the
            beginning ensures that there are no variables with the same name (in
            python variables cannot start with dots) and will allow us to recognize
            registers in the compiler.
            """

        if not hasattr(Compiler.fresh_name, 'last'):
            Compiler.fresh_name.last = 0

        names = [".c{}".format(Compiler.fresh_name.last + i) for i in range(tot)]
        Compiler.fresh_name.last += tot

        if tot == 1:
            return names[0]

        return names

    def declare_strings(self, assigns):
        decl = set()
        for assign in assigns:
            name = assign.targets[0].name
            string = assign.expr.content[1:-1]
            length = len(string) + 1
            define = "".join([
                "@{} = ".format(name),
                "private unnamed_addr constant [{} x i8]".format(length),
                "c\"{}\\00\", align 1" .format(string)
            ])
            decl.add(define)
        return "\n".join(decl)

    def var_strings(self):

        strings = set()
        for var in self.free_vars_names:
            name = var.name
            define = "".join([
                "@.var_{} = ".format(name),
                "private unnamed_addr constant [{} x i8]".format(len(name) + 1),
                "c\"{}\\00\", align 1" .format(name)
            ])
            strings.add(define)
        return "\n".join(strings)

    def compile_node(self, node):
        """Creates a LLVM_instruction node from an AST node."""

        method_name = "compile_" + node.to_snake_case()
        if hasattr(self, method_name):
            return getattr(self, method_name)(node)
        else:
            raise NotCompilableError(node, node.line)

    def to_llvm(self, program):
        """Compile a list of flattened ast nodes into llvm."""

        # Checks that program is actually AST.Program
        if not isinstance(program, Ast.Program):
            raise NotAProgramError(program)

        # get current working directory
        cwd = path.dirname(path.abspath(getfile(currentframe())))
        base_path = cwd + "/runtime/"
        base_files = [
                "base_list",
                "base_operations",
                "base_strings",
                "base_tags",
                "base_runtime",
                "base_closure",
                "base_methods",
                "base"
                ]

        # Read all headers from base files
        base = ""
        for name in base_files:
            with open(base_path + name, 'r') as base_file:
                base += base_file.read()

        # Code for variables allocation + actual program code
        allocation = "\n\t".join(self.allocate_vars(program)) + "\n"

        # Compile functions
        decl = []
        for function in program.functions:
            decl += ["".join(self.compile_node(function))]
        decl = "\n".join(decl)

        # Define all needed strings
        vars_decl = self.var_strings() + "\n"
        vars_decl += self.declare_strings(program.literals) + "\n"

        # Compile statements
        code = []
        for statement in program:
            code += ["\t" + "".join(self.compile_node(statement))]
        code = "\n".join(code)

        # Return compiled program in base template
        return base.format(vars_decl + decl, allocation + code)


def compile(program):
    compiler = Compiler()
    return compiler.to_llvm(program)
