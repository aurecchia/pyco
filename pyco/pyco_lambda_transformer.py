#!/usr/bin/env python

__author__ = "Argenti Marco, Aulecchia Alessio, Azara Antonio"

from nodes.AST import *


def convert_lambdas_to_function(program):
    for i in range(len(program.functions)):

        if isinstance(program.functions[i], Lambda):
            lam = program.functions[i]
            body = [Return(lam.code, lam.line)]
            converted = Function(lam.name, lam.args, body, lam.line)
            program.functions[i] = converted



    return program
