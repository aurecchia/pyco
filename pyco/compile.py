#!/usr/bin/env python

import sys
import logging
from optparse import OptionParser

import pyco_parser_generator as parser
import pyco_flattener as flattener
import pyco_compiler as compiler
import pyco_lifter as lifter
import pyco_string_lifter as literal_lifter
import pyco_lambda_transformer as transformer
from nodes.AST import PyCoException


__author__ = "Argenti Marco, Aurecchia Alessio, Azara Antonio"


def log_error(error):
    error_string = error.__repr__()
    if hasattr(error, "line"):
        error_string += "  > " + program.splitlines(True)[error.line - 1]
    logging.error(error_string)
    sys.exit(1)


if __name__ == '__main__':
    program = ""

    opt_parser = OptionParser(usage="usage:%prog [options] program")

    opt_parser.add_option("-p", "--parsed", action="store_true",
                            dest="print_parsed", default=False,
                            help="Print the parsed program (AST).")
    opt_parser.add_option("-l", "--lifted", action="store_true",
                          dest="print_lifted", default=False,
                          help="Print the program with lifted functions (AST).")
    opt_parser.add_option("-t", "--transformed", action="store_true",
                          dest="print_transformed", default=False,
                          help="Print program with transformed lambdas (AST).")
    opt_parser.add_option("-a", "--associated", action="store_true",
                          dest="print_associated", default=False,
                          help="Print program after associating vars (AST).")
    opt_parser.add_option("-f", "--flattened", action="store_true",
                            dest="print_flattened", default=False,
                            help="Print the flattened program (AST).")
    opt_parser.add_option("-n", "--nocompiled", action="store_false",
                            dest="print_compiled", default=True,
                            help="Do not print the compiled program (LLVM).")

    (options, args) = opt_parser.parse_args()

    try:
        # Checks for input argument
        if len(args) != 1:
            opt_parser.error("Missing program to compile!")
        with open(args[0], 'r') as python_file:
            program = python_file.read()

        # Scan and parse program to get AST
        parsed = parser.parse(program)
        if options.print_parsed:
            print "=== PARSED ============================="
            print parsed
            print

        lifted = lifter.lift_functions(parsed)
        if options.print_lifted:
            print "=== LIFTED ============================="
            print lifted
            print

        literal_lifted = literal_lifter.lift_literals(lifted)
        if options.print_lifted:
            print "=== STRINGS LIFTED ====================="
            print literal_lifted
            print

        transformed = transformer.convert_lambdas_to_function(literal_lifted)
        if options.print_transformed:
            print "=== TRANSFORMED LAMBDAS ================"
            print transformed
            print

        for function in transformed.functions:
            for arg in function.args:
                arg.name = "." + arg.name

        # Flattens the AST to make compilation easier
        flattened = flattener.flatten_program(transformed)
        if options.print_flattened:
            print "=== FLATTENED =========================="
            print flattened
            print

        # Compiles the flattened code
        compiled = compiler.compile(flattened)
        if options.print_compiled:
            print compiled

    except PyCoException as Error:
        log_error(Error)
