from nodes.AST import *
from nodes.AST import PyCoException


class NotConvertibleError(PyCoException):
    """ Raised when trying to create closures for unsupported objects. """

    def __init__(self, obj, line):
        self.obj = obj
        self.line = line

    def __repr__(self):
        return "Object {} cannot be closure converted!".format(repr(self.obj))


class UnsupportedFunctionOperation(PyCoException):
    """ Raised when trying to create closures for unsupported objects. """

    def __init__(self, obj, line):
        self.obj = obj
        self.line = line

    def __repr__(self):
        name = self.obj.__class__.__name__
        return "{} cannot be applied to a Lambda!".format(name)


class Lifter():
    """ Takes all functions and transforms them in first level functions. """

    def __init__(self):
        self.in_function = 0

    def fun_fresh_name(self, name):
        if not hasattr(self, "last_name"):
            self.last_name = 0
        else:
            self.last_name += 1

        return ".fun{}_{}".format(self.last_name, name)

    def is_free(self, node, bound):
        return self.in_function > 0 and \
               isinstance(node, Identifier) and \
               not node.name in bound

    def lambda_closure(self, node, bound):
        self.in_function += 1
        free = []

        new_bound = set()
        for arg in node.args:
            new_bound.add(arg.name)

        if isinstance(node.code, Lambda):
            closure, to_lift, free_s = self.lambda_closure(node.code, new_bound)

            to_lift.append(node.code)
            node.code = closure

        elif self.is_free(node.code, new_bound):
            to_lift = []
            free_s = [node.code]
            node.code = GetEnvVar(node.code, node.line)

        else:
            to_lift, free_s = self.lift(node.code, new_bound)

        node.name = self.fun_fresh_name(node.name)

        env = MakeEnv(node.line)
        for var in free_s:
            if var.name in bound:
                env.vars.append(var)
            else:
                free.append(var)
                env.vars.append(GetEnvVar(var, node.line))

        new_name = self.fun_fresh_name(node.name)

        argc = len(node.args)
        closure = MakeClosure(new_name, node.name, argc, env, node.line)
        node.name = new_name
        self.in_function -= 1
        return closure, to_lift, free

    def function_closure(self, node, bound):
        free = []

        new_bound = set()
        for arg in node.args:
            new_bound.add(arg.name)
        bound.add(node.name)

        to_lift, free_s = self.lift(node, new_bound)

        env = MakeEnv(node.line)
        for var in free_s:
            if var.name in bound:
                env.vars.append(var)
            else:
                free.append(var)
                env.vars.append(GetEnvVar(var, node.line))

        new_name = self.fun_fresh_name(node.name)

        argc = len(node.args)
        ident = Identifier(node.name, node.line)
        closure = MakeClosure(new_name, node.name, argc, env, node.line)
        node.name = new_name
        return Assign([ident], closure, node.line), to_lift, free

    def lift_unary(self, unary, bound):
        if isinstance(unary.expr, Lambda):
            closure, to_lift, free = self.lambda_closure(unary.expr, bound)

            to_lift.append(unary.expr)
            unary.left = closure

            return to_lift, free

        elif self.is_free(unary.expr, bound):
            free = [unary.expr]
            unary.expr = GetEnvVar(unary.expr, unary.line)

            return [], free

        else:
            return self.lift(unary.expr, bound)

    def lift_binary(self, binary, bound):
        to_lift, free = [], []

        if isinstance(binary.left, Lambda):
            closure, to_lift, free = self.lambda_closure(binary.left, bound)

            to_lift.append(binary.left)
            binary.left = closure

        elif self.is_free(binary.left, bound):
            free.append(binary.left)
            binary.left = GetEnvVar(binary.left, binary.line)

        else:
            to_lift, free = self.lift(binary.left, bound)

        if isinstance(binary.right, Lambda):
            closure, to_lift_r, free_r = self.lambda_closure(
                binary.right, bound)

            to_lift.append(binary.right)
            binary.right = closure

            to_lift += to_lift_r
            free += free_r

        elif self.is_free(binary.right, bound):
            free.append(binary.right)
            binary.right = GetEnvVar(binary.right, binary.line)

        else:
            r_lift, r_free = self.lift(binary.right, bound)
            to_lift += r_lift
            free += r_free

        return to_lift, free

    def lift_boolean_binary(self, binary, bound):
        return self.lift_binary(binary, bound)

    def lift_assign(self, assign, bound):
        for target in assign.targets:
            bound.add(target.name)

        if isinstance(assign.expr, Lambda):
            closure, to_lift, free = self.lambda_closure(assign.expr, bound)

            to_lift.append(assign.expr)
            assign.expr = closure

            return to_lift, free

        elif self.is_free(assign.expr, bound):
            free = [assign.expr]
            assign.expr = GetEnvVar(assign.expr, assign.line)

            return [], free

        else:
            to_lift, free = self.lift(assign.expr, bound)
            return to_lift, free

    def lift_augmented_assign(self, assign, bound):
        bound.add(assign.target.name)

        if isinstance(assign.expr, Lambda):
            raise UnsupportedFunctionOperation(assign, assign.line)

        elif self.is_free(assign.expr, bound):
            free = [assign.expr]
            assign.expr = GetEnvVar(assign.expr, assign.line)

            return [], free

        else:
            to_lift, free = self.lift(assign.expr, bound)
            return to_lift, free

    def lift_if(self, if_s, bound):
        if isinstance(if_s.cond, Lambda):
            raise UnsupportedFunctionOperation(if_s, if_s.line)

        elif self.is_free(if_s.cond, bound):
            to_lift, free = [], [if_s.cond]
            if_s.cond = GetEnvVar(if_s.cond, if_s.line)

        else:
            to_lift, free = self.lift(if_s.cond, bound)

        lift_if, free_if = self.lift(if_s.body, bound)
        lift_else, free_else = self.lift(if_s.else_body, bound)

        to_lift += lift_if + lift_else
        free += free_if + free_else

        return to_lift, free

    def lift_while(self, while_s, bound):
        if isinstance(while_s.cond, Lambda):
            raise UnsupportedFunctionOperation(while_s, while_s.line)

        elif self.is_free(while_s.cond, bound):
            to_lift, free = [], [while_s.cond]
            while_s.cond = GetEnvVar(while_s.cond, while_s.line)

        else:
            to_lift, free = self.lift(while_s.cond, bound)

        lift_body, free_body = self.lift(while_s.body, bound)

        to_lift += lift_body
        free += free_body

        return to_lift, free

    def lift_function(self, function, bound):
        self.in_function += 1
        to_lift, free = self.lift(function.code, bound)

        self.in_function -= 1
        return to_lift, free

    def lift_lambda_container(self, container, bound):
        closure, to_lift, free = self.lambda_closure(container.function, bound)

        to_lift += [container.function]
        container.function = closure
        return to_lift, free

    def lift_function_call(self, call, bound):
        to_lift = []
        free = []
        name = []
        if isinstance(call.name, Lambda):
            closure, to_lift, free = self.lambda_closure(call.name, bound)

            to_lift.append(call.name)
            call.name = closure

        elif self.is_free(call.name, bound):
            name = [call.name]
            call.name = GetEnvVar(call.name, call.line)

        else:
            to_lift_c, free_c = self.lift(call.name, bound)
            to_lift += to_lift_c
            free += free_c

        to_lift_c, free_c = self.lift(call.args, bound)
        return to_lift + to_lift_c, name + free + free_c

    def lift_print(self, call, bound):
        return self.lift(call.args, bound)

    def lift_return(self, ret, bound):
        name = []
        if isinstance(ret.value, Lambda):
            closure, to_lift, free = self.lambda_closure(ret.value, bound)

            to_lift.append(ret.value)
            ret.value = closure

            return to_lift, free

        if self.is_free(ret.value, bound):
            name = [ret.value]
            ret.value = GetEnvVar(ret.value, ret.line)

        to_lift, free = self.lift(ret.value, bound)
        return to_lift, name + free

    def lift_program(self, program, bound):
        to_lift, free = self.lift(program.statements, bound)
        program.functions = to_lift

        return program, free

    def lift_discard(self, discard, bound):
        if isinstance(discard.expr, Lambda):
            return [], []

        return self.lift(discard.expr, bound)

    def list_list(self, node, bound):
        to_lift, free = self.lift(node.elems, bound)

        return to_lift, free

    def lift_statements(self, statements, bound):
        to_lift = []
        free = []
        for i in range(len(statements)):
            statement = statements[i]

            if isinstance(statement, Function):
                closure, f_lift, f_free = self.function_closure(statement,
                                                                bound)
                to_lift += [statement]
                to_lift += f_lift
                free += f_free
                statements[i] = closure

            elif isinstance(statement, Lambda):
                closure, f_lift, f_free = self.lambda_closure(statement, bound)
                to_lift += [statement]
                to_lift += f_lift
                free += f_free
                statements[i] = closure

            elif self.is_free(statement, bound):
                statements[i] = GetEnvVar(statement, statement.line)
                free.append(statement)

            else:
                to_lift_s, free_s = self.lift(statement, bound)
                to_lift += to_lift_s
                free += free_s

        return to_lift, free

    def lift(self, node, bound):
        if isinstance(node, list):
            return self.lift_statements(node, bound)

        if not node is None:
            method_name = "lift_" + node.to_snake_case()
        else:
            return [], []

        if hasattr(self, method_name):
            return getattr(self, method_name)(node, bound)
        else:
            return [], []


def lift_functions(program):
    lifter = Lifter()
    return lifter.lift(program, set())[0]

