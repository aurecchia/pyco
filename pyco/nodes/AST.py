# Supporte operations

from re import sub


class PyCoException(Exception):
    def __repr__(self):
        return "PyCo Exception!"


def get_env_name():

    if not hasattr(get_env_name, 'last'):
        get_env_name.last = 0
    get_env_name.last += 1
    return "env{}".format(get_env_name.last - 1)


def label_name():
    if not hasattr(label_name, 'last'):
        label_name.last = 0
    else:
        label_name.last += 1

    return label_name.last


class UndefinedOperatorError(PyCoException):
    """ Raised when creating a node for an unsupported operation. """

    def __init__(self, operation):
        self.operation = operation
        self.line = operation.line

    def __repr__(self):
        return "Operation {} not supported!".format(repr(self.operation))


class UndefinedTypeError(PyCoException):
    """ Raised when trying to tag with an unknown type. """

    def __init__(self, wrong_type, line):
        self.type = wrong_type
        self.line = line

    def __repr__(self):
        return "Type {} is undefined!".format(repr(self.type))


class OutOfScopeParentException(PyCoException):

    def __init__(self, name, line):
        self.name = name
        self.line = line

    def __repr__(self):
        return "Class {} cannot be extended because it is in a different scope, or non existent.".format(self.name)


class AstNode:
    """ Class representing a generic AST node. """

    def __init__(self, line=None):
        self.line = line

    def to_snake_case(self):
        # Taken from: http://stackoverflow.com/questions/1175208
        name = self.__class__.__name__
        s1 = sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        return sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


class Binary(AstNode):
    """ Class representing a binary operation. """

    # The set of supported binary operations
    operators = {'+', '-', '*', '/', '//', '%', '&', '^', '|', '**', '<<', '>>'}

    # The set of suported comparisons
    comparisons = {">", ">=", "<", "<=", "==", "!="}

    def __init__(self, operator, left, right, line=None):
        AstNode.__init__(self, line)
        if operator not in self.operators and operator not in self.comparisons:
            raise UndefinedOperatorError(operator)

        self.operator = operator
        self.left = left
        self.right = right

    def is_bitwise(self):
        return self.operator in ('&', '|', '^')

    def is_comparison(self):
        return self.operator in self.comparisons

    def __repr__(self):
        return "({} {} {})".format(repr(self.left), self.operator,
                                   repr(self.right))


class BooleanBinary(AstNode):
    """ Class representing a boolean binary operation (and/or). """

    operators = {"and", "or"}

    def __init__(self, operator, left, right, line=None):
        AstNode.__init__(self, line)

        if operator not in self.operators:
            raise UndefinedOperatorError(operator)

        self.operator = operator
        self.left = left
        self.right = right

    def __repr__(self):
        return "({} {} {})".format(repr(self.left), self.operator,
                                   repr(self.right))


class Unary(AstNode):
    """ Class representing an unary operation. """

    # The set of supported unary operations
    operators = {'+', '-', '~', 'not'}

    def __init__(self, operator, expr, line=None):
        AstNode.__init__(self, line)
        if operator not in self.operators:
            raise UndefinedOperatorError(operator)

        self.operator = operator
        self.expr = expr

    def __repr__(self):
        return "{}{}".format(self.operator, repr(self.expr))


class AugmentedAssign(AstNode):
    """ Class representing augmented assignments. """

    # The set of supported augmented assignments
    operators = {'+=', '-=', '*=', '/=', '**=', '//=', '%=', '|=', '^=', '&=',
                 '<<=', '>>='}

    def __init__(self, target, operator, expr, line=None):
        AstNode.__init__(self, line)
        if operator not in self.operators:
            raise UndefinedOperatorError(operator)

        self.target = target
        self.operator = operator
        self.expr = expr

    def __repr__(self):
        return "{} {} {}".format(self.target, self.operator, repr(self.expr))


class Assign(AstNode):
    """ Class representing an assignment. """

    def __init__(self, targets, expr, line=None):
        AstNode.__init__(self, line)
        if not isinstance(targets, list):
            msg = "Targets of Ast.Assign must be a list! Got {}".format(
                targets.__class__)
            raise PyCoException(msg)
        self.targets = targets
        self.expr = expr

    def __repr__(self):
        to = [repr(n) for n in self.targets]
        x = "{} = {}".format(" = ".join(to), repr(self.expr))
        return x


class Integer(AstNode):
    """ Class representing an integer. """

    def __init__(self, value, line=None):
        AstNode.__init__(self, line)
        self.value = int(value)

    def __repr__(self):
        return repr(self.value)


class Boolean(AstNode):
    """ Class representing a boolean. """

    def __init__(self, value, line=None):
        AstNode.__init__(self, line)
        self.value = value

    def __repr__(self):
        if self.value:
            return "True"
        else:
            return "False"


class String(AstNode):
    """ Represents strings. """

    def __init__(self, content, line):
        AstNode.__init__(self, line)
        self.reference = None
        self.content = content

    def __repr__(self):
        return "STRING({})".format(self.content)


class List(AstNode):
    """Represents lists."""

    specific_functions = {"append", "extend", "insert", "remove", "pop",
                          "reverse", "put", "put_list"}

    def __init__(self, elems, line):
        AstNode.__init__(self, line)
        self.elems = elems

    def __repr__(self):
        return repr(self.elems)


class Identifier(AstNode):
    """ Class representing an identifier. """

    def __init__(self, name, line=None):
        AstNode.__init__(self, line)
        if not isinstance(name, str):
            raise PyCoException(name.__class__)
        self.name = name

    def is_register(self):
        """Tells whether the identifier is for a register."""
        return self.name[0] == "."

    def __repr__(self):
        return self.name


class NoneRef(AstNode):
    """ Class representing a None value. """

    def __init__(self, line):
        AstNode.__init__(self, line)

    def __repr__(self):
        return "NONE"


class Program(AstNode):
    """ Class representing a program."""

    def __init__(self, statements, functions=[], literals = [], line=None):
        AstNode.__init__(self, line)

        self.statements = statements
        self.functions = functions
        self.literals = literals

    def __len__(self):
        return len(self.statements)

    def __iter__(self):
        return iter(self.statements)

    def __repr__(self):
        lits = [repr(l) for l in self.literals]
        funs = [repr(f) for f in self.functions]
        stats = [repr(x) for x in self.statements]
        return "\n".join(funs + lits + stats)


class Print(AstNode):
    """ Class representing a print statement. """

    def __init__(self, args=[], newline=True, prompt=False, line=None):
        AstNode.__init__(self, line)
        self.args = args
        self.newline = newline
        self.prompt = prompt

    def __repr__(self):
        args = ", ".join([repr(arg) for arg in self.args])
        if self.newline:
            sep = "_nl"
        else:
            sep = ""
        return "Print{}({})".format(sep, args)


class Input(AstNode):
    """ Class representing an input statement. """

    def __init__(self, args=None, line=None):
        AstNode.__init__(self, line)
        self.args = args

    def __repr__(self):
        if self.args:
            return "Input({})".format(repr(self.args))
        else:
            return "Input()"


class Discard(AstNode):
    """ Class representing a statement not assigned to any variable. """

    def __init__(self, expr, line=None):
        AstNode.__init__(self, line)
        self.expr = expr

    def __repr__(self):
        return "DISCARD({})".format(repr(self.expr))


class Label(AstNode):
    """ Class representing a label reachable by a GOTO statement. """

    def __init__(self, line=None):
        AstNode.__init__(self, line)
        self.name = "L{}".format(label_name())

    def __repr__(self):
        return "LABEL({})".format(repr(self.name))


class Goto(AstNode):
    """ Class representing a goto statement to reach a label. """

    def __init__(self, label, line=None):
        AstNode.__init__(self, line)
        self.label = label

    def __repr__(self):
        return "GOTO({})".format(repr(self.label))


class If(AstNode):
    """ Class representing an if statement. """

    def __init__(self, cond, body, else_body=[], line=None):
        AstNode.__init__(self, line)
        self.cond = cond
        self.body = body
        self.else_body = else_body

    def __repr__(self):
        s_repr = "IF {} : \n \t {} )".format(repr(self.cond), repr(self.body))
        if self.else_body:
            s_repr += "\n \t ELSE : {} ".format(repr(self.else_body))
        return s_repr


class While(AstNode):
    """ Class representing a while loop. """

    def __init__(self, cond, body, line=None):
        AstNode.__init__(self, line)
        self.cond = cond
        self.body = body

    def __iter__(self):
        return iter(self.body)

    def __repr__(self):
        return "WHILE {} :\n DO : {} ".format(repr(self.cond), repr(self.body))


class CompatibleOperation(AstNode):
    """ Class representing compatible binary operations. """

    def __init__(self, left, right, op,  line):

        AstNode.__init__(self, line)

        self.left = left
        self.right = right
        self.op = op

        def __repr__(self):
            return "COP {} {} {} ".format(self.left, self,op, self.right)


class ValidOperation(AstNode):
    """ Class representing valid unary operations. """

    def __init__(self, operand, operator, line):

        AstNode.__init__(self, line)

        self.operand = operand
        self.operator = operator

    def __repr__(self):
        return "VOP {} {}".format(self.operator, self.operand)


class Tag(AstNode):
    """ Represents a tag statement. """

    def __init__(self, expr, type, line=None):
        AstNode.__init__(self, line)

        if not type in Types:
            raise UndefinedTypeError(type, line)

        self.expr = expr
        self.type = type.__name__
        self.type_code = Types.get(type)

    def __repr__(self):
        return "TAG_{}({})".format(self.type, repr(self.expr))


class SafeTag(AstNode):
    """ Represents a tag statement with check on the operands' types. """

    def __init__(self, expr, left, right, line=None):
        AstNode.__init__(self, line)

        self.expr = expr
        self.left = left
        self.right = right

    def __repr__(self):
        return "C_TAG({} {})".format(self.left, self.right)


class Cast32(AstNode):
    """Represents a cast operation from i1 to i32. """

    def __init__(self, value, line=None):
        AstNode.__init__(self, line)

        self.value = value

    def __repr__(self):
        return "CAST32({})".format(self.value)


class Cast1(AstNode):
    """Represents a cast operation from i32 to i1. """

    def __init__(self, value, line=None):
        AstNode.__init__(self, line)

        self.value = value

    def __repr__(self):
        return "CAST1({})".format(self.value)


class Cast(AstNode):

    def __init__(self, value, type, line=None):
        AstNode.__init__(self, line)

        self.value = value
        self.type = type

    def __repr__(self):
        return "CAST_{}({})".format(self.type, repr(self.value))


class Untag(AstNode):
    """ Represents an untag statement. """

    def __init__(self, expr, expected=None, line=None):
        AstNode.__init__(self, line)

        if expected and not expected in Types:
            raise UndefinedTypeError(expected, line)

        self.expr = expr
        self.expected = None
        if expected:
            self.expected = expected.__name__

    def __repr__(self):
        if self.expected:
            return "UNTAG_{}({})".format(self.expected, self.expr)
        else:
            return "UNTAG({})".format(self.expr)


class ClassDef(AstNode):
    """Represents a class definition."""

    class_scope = set()

    def __init__(self, name, parents, body, line=None):
        AstNode.__init__(self, line)

        for parent in parents:
            if parent.name not in ClassDef.class_scope:
                raise OutOfScopeParentException(parent.name, line)

        self.name = name
        self.parents = parents
        self.body = body

        ClassDef.class_scope.add(self.name)

    def __repr__(self):
        parents = ", ".join([repr(self.parents)])
        body = "\n\t".join([repr(statement) for statement in self.body])
        return "CLASS {}({}): \n\t{}\n".format(self.name, parents, body)


class Function(AstNode):
    """Represents a function definition."""

    def __init__(self, name, args, code, line=None):
        AstNode.__init__(self, line)

        self.name = name
        self.args = args
        self.code = code

    def __iter__(self):
        return iter(self.code)

    def __repr__(self):
        arg_string = ", ".join([repr(n) for n in self.args])
        lines = "\n\t".join([repr(n) for n in self.code])

        return "FUNC {} ({}):\n\t{}\n".format(self.name, arg_string, lines)


class FunctionCall(AstNode):
    """ Represents a function call. """

    def __init__(self, name, args, line):
        AstNode.__init__(self, line)

        self.name = name
        self.args = args

    def __repr__(self):
        args = ", ".join([repr(arg) for arg in self.args])
        return "{}({})".format(self.name, args)


class RuntimeCall(FunctionCall):

    def __init__(self, name, args, ret, line):
        FunctionCall.__init__(self, name, args, line)

        self.ret = ret


class MethodCall(AstNode):
    """Represents a method call."""

    def __init__(self, caller, name, args, line):
        AstNode.__init__(self, line)

        self.caller = caller
        self.name = name
        self.args = args

    def __repr__(self):
        args = ", ".join([repr(arg) for arg in self.args])
        return "{}.{}({})".format(self.caller, self.name, args)


class Lambda(AstNode):
    """ Represents a lambda function. """

    def __init__(self, args, code, line):
        AstNode.__init__(self, line)

        self.name = lambda_fresh_name()
        self.args = args
        self.code = code

    def __repr__(self):
        arg_string = ", ".join([repr(n) for n in self.args])
        return "LAMBDA {}: {}".format(arg_string, self.code)


class LambdaContainer(AstNode):
    """ Represents a lambda function and a lambda call. """

    def __init__(self, ast_lambda, ast_call, line):
        AstNode.__init__(self, line)

        self.function = ast_lambda
        self.call = ast_call

    def __repr__(self):
        return "(LAMBDA_CONTAINER({}; {})".format(repr(self.function), repr(self.call))


class Return(AstNode):
    """ Represent a return statement."""

    def __init__(self, value, line):
        AstNode.__init__(self, line)

        self.value = value

    def __repr__(self):
        return "return {}".format(repr(self.value))


class MakeEnv(AstNode):
    """ Represents a call the runtime function to make the environment. """

    def __init__(self, line):
        AstNode.__init__(self, line)

        self.closure = None
        self.vars = []
        self.key = None

    def __repr__(self):
        return "MAKE_ENV({})".format(repr(self.vars))


class GetEnvVar(AstNode):
    """ Represents a call to get a variable from a runtime variable. """

    def __init__(self, var, line):
        AstNode.__init__(self, line)

        self.var = var

    def __repr__(self):
        return "GET_ENV_VAR({})".format(self.var)


class MakeClosure(AstNode):
    """ Represents a call to create the closure of a function. """

    def __init__(self, function_name, old_name, argc, env, line):
        AstNode.__init__(self, line)
        self.function_name = function_name
        self.old_name = old_name
        self.argc = argc
        self.env = env

    def __repr__(self):
        name = repr(self.function_name)
        env = repr(self.env)
        return "MAKE_CLOSURE({},{})".format(name, env)


class IntToPointer(AstNode):

    def __init__(self, closure_reg, dest_type, line):
        AstNode.__init__(self, line)

        self.closure_reg = closure_reg
        self.dest_type = dest_type

    def __repr__(self):
        return "INT_TO_PTR_({}, {})".format(self.dest_type,
                                            repr(self.closure_reg))


def lambda_fresh_name():
    #make a name for lambda function
    if not hasattr(lambda_fresh_name, 'last'):
        lambda_fresh_name.last = 0

    lambda_fresh_name.last += 1
    return "$lambda{}".format(lambda_fresh_name.last - 1)





Types = {Integer: 0, Boolean: 1, NoneRef: 3}



