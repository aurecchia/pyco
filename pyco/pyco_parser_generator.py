#!/usr/bin/env python

__author__ = "Argenti Marco, Aulecchia Alessio, Azara Antonio"

import ply.yacc as yacc
from pyco_scanner import tokens
from pyco_scanner import init_lexer
import nodes.AST as Ast
from nodes.AST import PyCoException

# Precedence of operations
precedence = (
    ("nonassoc", "LAMBDA"),
    ("left", "OR"),
    ("left", "AND"),
    ("nonassoc", "NOT"),
    ("nonassoc", "EQ", "NEQ", "NEQ2", "LT", "GT", "LEQ", "GEQ"),
    ("left", "BITOR"),
    ("left", "BITXOR"),
    ("left", "BITAND"),
    ("left", "LSHIFT", "RSHIFT"),
    ("left", "ADD", "SUB"),
    ("left", "MUL", "DIV", "FDIV", "MOD"),
    ("nonassoc", "INV"),
    ("right", "POW")
)


class ParserSyntaxError(PyCoException):
    """Exception raised when a syntax error is found.

    If the reason of the error is known, the exception tries to return an
    explanation.
    """
    errors = {
        "FLOAT": "Floats are not supported!",
        "INDENT": "Bad indentation!"
    }

    def __init__(self, token, line):
        self.token = token
        self.line = line

    def __repr__(self):
        error = "\n[!] Syntax error at line {}!\n".format(self.line)
        if self.token in self.errors:
            error += "  > " + self.errors.get(self.token) + "\n"
        return error


# The program is composed of lines
def p_program(p):
    """program : lines"""
    p[0] = Ast.Program(p[1], line=p.lineno(1))


# Lines could be a statement followed by other lines
def p_line(p):
    """lines : statement lines"""
    temp = [p[1]]
    temp.extend(p[2])
    p[0] = temp


# Lines could be an empty line followed by other lines
def p_line_newline(p):
    """lines : NEWLINE lines"""
    p[0] = p[2]


# Stopping recursion
def p_line_empty(p):
    """lines : """
    p[0] = []


# A statement could be an assignment, an augmented assignment, a print call,
# an if, a loop or a function call
def p_statement(p):
    """statement : assignment NEWLINE
                |   aug_assignment NEWLINE
                |   print NEWLINE
                |   return
                |   if
                |   while
                |   function"""
    p[0] = p[1]


# A statement could also be discarded
def p_statement_discard(p):
    """statement : expression NEWLINE"""
    p[0] = Ast.Discard(p[1], p.lineno(1))


# A binary boolean expression.
def p_binary_conditional(p):
    """expression : expression AND expression
                |   expression OR expression"""
    p[0] = Ast.BooleanBinary(p[2], p[1], p[3], p.lineno(2))


def p_expression_lambda(p):
    """expression : lambda
                |   index_slice
                |   slice"""
    p[0] = p[1]


# An expression could be one of the following operations on another expression
# and a term
def p_expression_term_operations(p):
    """expression : expression EQ expression
                | expression NEQ expression
                | expression NEQ2 expression
                | expression LT expression
                | expression GT expression
                | expression LEQ expression
                | expression GEQ expression
                | expression BITOR expression
                | expression BITXOR expression
                | expression BITAND expression
                | expression LSHIFT expression
                | expression RSHIFT expression
                | expression ADD expression
                | expression SUB expression
                | expression MUL expression
                | expression DIV expression
                | expression MOD expression
                | expression FDIV expression
                | expression POW expression"""

    p[0] = Ast.Binary(p[2], p[1], p[3], p.lineno(2))


# A factor could be one of the following operations on another factor
def p_term_unary_operator(p):
    """term : ADD term
            | SUB term
            | INV term
            | NOT term"""
    p[0] = Ast.Unary(p[1], p[2], p.lineno(1))


# A term could be a factor
def p_term_factor(p):
    """expression : term"""
    p[0] = p[1]


# An expression could be a string
def p_string(p):
    """expression : STRING"""
    p[0] = Ast.String(p[1], p.lineno(1))


def p_list(p):
    """expression : list"""
    p[0] = p[1]


def p_expression_method_call(p):
    """expression : method_call"""
    p[0] = p[1]


# A factor could be a function call
def p_factor_function_call(p):
    """term : function_call"""
    p[0] = p[1]


# A factor could be a parenthesized expression
def p_expression_paren(p):
    """term : OPAREN expression CPAREN"""
    p[0] = p[2]


# A base could be an integer
def p_term_integer(p):
    """term : INTEGER"""
    p[0] = Ast.Integer(int(p[1]), p.lineno(1))


# A base could be a boolean
def p_term_true(p):
    """term : TRUE"""
    p[0] = Ast.Boolean(1, p.lineno(1))


# A base could be a boolean
def p_term_false(p):
    """term : FALSE"""
    p[0] = Ast.Boolean(0, p.lineno(1))


def p_term_none(p):
    """term : NONE"""
    p[0] = Ast.NoneRef(p.lineno(1))


# A base could be an identifier
def p_factor_identifier(p):
    """term : IDENTIFIER"""
    p[0] = Ast.Identifier(p[1], p.lineno(1))


# An assignment is an identifier followed by either other assignment_ids
# The last element of assignment_ids will be the actual expression
def p_assignment(p):
    """assignment : IDENTIFIER ASSIGN assignment_ids"""
    ids = [Ast.Identifier(p[1], p.lineno(1))]
    ids.extend(p[3][:-1])
    p[0] = Ast.Assign(ids, p[3][-1], p.lineno(3))


# Assignments_ids could be an identifier plus an equal sign followed by other
# assignment_ids
def p_assignment_ids(p):
    """assignment_ids : IDENTIFIER ASSIGN assignment_ids"""
    ids = [Ast.Identifier(p[1], p.lineno(1))]
    ids.extend(p[3])
    p[0] = ids


# assignment_ids could be an expression
def p_assignment_ids_empty(p):
    """assignment_ids : expression"""
    p[0] = [p[1]]


# An aug_assignment is an identifier followed by an augmented assignment
# operator and by an expression
def p_aug_assignment_add(p):
    """aug_assignment : IDENTIFIER AUG_ASSIGN expression"""
    identifier = Ast.Identifier(p[1], p.lineno(1))
    p[0] = Ast.AugmentedAssign(identifier, p[2], p[3], p.lineno(2))


# A print is a print statement followed by print_args
def p_function_call_print(p):
    """print : PRINT arguments """
    p[0] = Ast.Print(p[2], True, line=p.lineno(2))


# A print is a print statement followed by print_args
def p_function_call_print_nonewline(p):
    """print : PRINT expression COMMA """
    p[0] = Ast.Print([p[2]], False, line=p.lineno(2))


def p_expression_fun_call(p):
    """function_call : callable_expression OPAREN arguments CPAREN"""
    if isinstance(p[1], Ast.Identifier) and p[1].name == "input":
        if p[3] is []:
            p[0] = Ast.Input(None, p.lineno(1))
        else:
            p[0] = Ast.Input(p[3], p.lineno(1))

    elif isinstance(p[1], Ast.Identifier) and p[1].name == "len":
        method = Ast.Identifier("length", p.lineno(3))
        p[0] = Ast.MethodCall(p[3][0], method, [], p.lineno(3))

    else:
        p[0] = Ast.FunctionCall(p[1], p[3], p.lineno(1))


def p_callable_expression(p):
    """callable_expression : function_call
                        |   method_call"""
    p[0] = p[1]


def p_callable_expression_paren(p):
    """callable_expression : OPAREN expression CPAREN"""
    p[0] = p[2]


def p_callable_expression_ident(p):
    """callable_expression : IDENTIFIER"""
    p[0] = Ast.Identifier(p[1], p.lineno(1))


def p_callable_expression_string(p):
    """callable_expression : STRING"""
    p[0] = Ast.String(p[1], p.lineno(1))


# Print_args could be an expression
def p_arguments(p):
    """arguments : expression"""
    p[0] = [p[1]]


# Print args could be an expression followed by a comma and other arguments
def p_arguments_comma(p):
    """arguments : expression COMMA arguments"""
    args = [p[1]]
    args.extend(p[3])
    p[0] = args


# Print_args could also be nothing
def p_arguments_empty(p):
    """arguments : """
    p[0] = []

#Lambda.
def p_lambda(p):
    """lambda : LAMBDA parameters COLON expression"""
    p[0] = Ast.Lambda(p[2], p[4], p.lineno(1))


# If construct.
def p_if(p):
    """if : IF expression COLON NEWLINE INDENT statement lines DEDENT if_rest"""
    cond = p[2]
    if_body = [p[6]]
    if_body.extend(p[7])
    else_body = []

    if p[9]:
        else_body = p[9]

    p[0] = Ast.If(cond, if_body, else_body, p.lineno(6))


# Separation between if branch and everything else
def p_if_rest_elif(p):
    """if_rest : elif if_rest"""
    if not p[2] or isinstance(p[2], list):
        p[1].else_body = p[2]
    else:
        p[1].else_body = [p[2]]
    p[0] = [p[1]]


# Rule for the else branch.
def p_if_rest_else(p):
    """if_rest : else"""
    p[0] = p[1]


# Body of the else part of if empty in case no else is there.
def p_if_rest_empty(p):
    """if_rest : """
    p[0] = None


# Else if statement structure defined as elif expr : body, with proper indentation.
def p_elif(p):
    """elif : ELIF expression COLON NEWLINE INDENT statement lines DEDENT"""
    #Assign parts of the if construct
    cond = p[2]
    if_body = [p[6]]
    if_body.extend(p[7])
    p[0] = Ast.If(cond, if_body, None, p.lineno(6))


# Else statement structure defined as else : body, with proper indentation.
def p_else(p):
    """else : ELSE COLON NEWLINE INDENT statement lines DEDENT"""
    #Assign part of the else construct
    body = [p[5]]
    body.extend(p[6])
    p[0] = body


# While statement structure defined as while expr : body, with proper indentation.
def p_while(p):
    """while : WHILE expression COLON NEWLINE INDENT statement lines DEDENT"""
    #Assign parts of the while construct
    cond = p[2]
    while_body = [p[6]]
    while_body.extend(p[7])
    p[0] = Ast.While(cond, while_body, p.lineno(6))


def p_list_lit(p):
    """list : OBRACKET arguments  CBRACKET"""
    p[0] = Ast.List(p[2], p.lineno(2))


def p_list_put(p):
    """expression : index_slice ASSIGN expression"""
    method = p[1]
    method.name = Ast.Identifier("put", p.lineno(1))
    method.args += [p[3]]
    p[0] = method


def p_list_slice_put(p):
    """expression : slice ASSIGN expression"""
    method = p[1]
    method.name = Ast.Identifier("list_put_list", p.lineno(1))
    method.args += [p[3]]
    p[0] = method


def p_get(p):
    """index_slice : callable_expression OBRACKET expression CBRACKET"""
    method = Ast.Identifier("get", p.lineno(1))
    p[0] = Ast.MethodCall(p[1], method, [p[3]], p.lineno(1))


def p_slice(p):
    """slice : callable_expression OBRACKET expression COLON expression CBRACKET"""
    method = Ast.Identifier("slice", p.lineno(1))
    p[0] = Ast.MethodCall(p[1], method, [p[3], p[5]], p.lineno(1))


def p_slice_up_to(p):
    """slice : callable_expression OBRACKET COLON expression CBRACKET"""
    method = Ast.Identifier("slice_up_to", p.lineno(1))
    p[0] = Ast.MethodCall(p[1], method, [p[4]], p.lineno(1))


def p_slice_from(p):
    """slice : callable_expression OBRACKET expression COLON CBRACKET"""
    method = Ast.Identifier("slice_from", p.lineno(1))
    p[0] = Ast.MethodCall(p[1], method, [p[3]], p.lineno(1))


def p_method_call(p):
    """method_call : callable_expression DOT IDENTIFIER OPAREN arguments CPAREN"""
    caller = p[1]
    method = Ast.Identifier(p[3], p.lineno(3))
    args = p[5]
    p[0] = Ast.MethodCall(caller, method, args, p.lineno(1))


def p_method_call_ident(p):
    """method_call : IDENTIFIER DOT IDENTIFIER OPAREN arguments CPAREN"""
    caller = Ast.Identifier(p[1], p.lineno(1))
    method = Ast.Identifier(p[3], p.lineno(3))
    args = p[5]
    p[0] = Ast.MethodCall(caller, method, args, p.lineno(1))

# Function is defined by a name and a list of arguments, finally it has its block of instructions
def p_function(p):
    """function : DEF IDENTIFIER OPAREN parameters CPAREN COLON NEWLINE INDENT statement lines DEDENT"""
    name_function = p[2]
    list_parameters = p[4]
    code_fun = [p[9]]
    code_fun.extend(p[10])

    p[0] = Ast.Function(name_function, list_parameters, code_fun, p.lineno(2))


# Return is just a statement defined by return expr.
def p_function_return(p):
    """return : RETURN expression NEWLINE"""
    p[0] = Ast.Return(p[2], p.lineno(2))


# Return is just a statement defined by return.
def p_function_return_empty(p):
    """return : RETURN NEWLINE"""
    p[0] = Ast.Return(Ast.NoneRef(p.lineno(1)), p.lineno(1))


# Parameters could be empty
def p_parameters_empty(p):
    """parameters : """
    p[0] = []


# Parameter could be empty
def p_parameter(p):
    """parameter : """
    p[0] = []


# Parameter could be a comma an identifier followed by another parameter
def p_parameter_list(p):
    """parameter : COMMA IDENTIFIER parameter"""
    temp = [Ast.Identifier(p[2], p.lineno(2))]
    temp.extend(p[3])
    p[0] = temp


# Parameters could be an identifier followed by another parameter
def p_parameters_list(p):
    """parameters : IDENTIFIER parameter"""
    temp = [Ast.Identifier(p[1], p.lineno(1))]
    temp.extend(p[2])
    p[0] = temp


# Called in case of errors during parsing
def p_error(p):
    raise ParserSyntaxError(p.type, p.lineno)


def parse(python_program):
    """Parse a python program.

    Returns a hierarchy of AST nodes representing the input program.
    """

    # Create parser and scan Python program
    parser = yacc.yacc()
    init_lexer()

    # Return parsed program
    return parser.parse(python_program)



