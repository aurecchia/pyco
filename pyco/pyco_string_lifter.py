
from nodes.AST import *


class StringLifter():
    """ Lift all constant string """

    def string_fresh_name(self):
        #make a name for strings
        if not hasattr(self, 'last'):
            self.last = 0

        self.last += 1
        return ".str{}".format(self.last - 1)

    def wrap_string(self, str, to_lift):
        ident = Identifier(self.string_fresh_name(), str.line)
        to_lift += [Assign([ident], str)]
        return ident

    def lift_unary(self, unary):
        if isinstance(unary.expr, String):
            to_lift = []
            unary.expr.reference = self.wrap_string(unary.expr, to_lift)
            return to_lift
        else:
            return self.lift(unary.expr)

    def lift_binary(self, binary):
        to_lift = []
        if isinstance(binary.left, String):
            binary.left.reference = self.wrap_string(binary.left, to_lift)
        else:
            to_lift = self.lift(binary.left)

        if isinstance(binary.right, String):
            binary.right.reference = self.wrap_string(binary.right, to_lift)
        else:
            to_lift += self.lift(binary.left)

        return to_lift

    def lift_boolean_binary(self, binary):
        return self.lift_binary(binary)

    def lift_assign(self, assign):
        if isinstance(assign.expr, String):
            to_lift = []
            assign.expr.reference = self.wrap_string(assign.expr, to_lift)
            return to_lift
        else:
            return self.lift(assign.expr)

    def lift_augmented_assign(self, assign):
        if isinstance(assign.expr, String):
            to_lift = []
            assign.expr.reference = self.wrap_string(assign.expr, to_lift)
            return to_lift
        else:
            return self.lift(assign.expr)

    def lift_if(self, if_s):
        to_lift = []
        if isinstance(if_s.cond, String):
            if_s.cond.reference = self.wrap_string(if_s.cond, to_lift)
        else:
            to_lift = self.lift(if_s.cond)

        to_lift.extend(self.lift(if_s.body))

        to_lift.extend(self.lift(if_s.else_body))

        return to_lift

    def lift_while(self, while_s):
        to_lift = []
        if isinstance(while_s.cond, String):
            while_s.cond.reference = self.wrap_string(while_s.cond, to_lift)
        else:
            to_lift = self.lift(while_s.cond)

        to_lift.extend(self.lift(while_s.body))
        return to_lift

    def lift_function(self, function):
        to_lift = self.lift(function.args)
        to_lift += self.lift(function.code)

        return to_lift

    def lift_lambda_container(self, container):
        to_lift = self.lift(container.function)
        to_lift += self.lift(container.call)

        return to_lift

    def lift_function_call(self, call):
        to_lift = []
        if isinstance(call.name, String):
            call.name.reference = self.wrap_string(call.name, to_lift)
        return to_lift + self.lift(call.args)

    def lift_method_call(self, call):
        to_lift = []
        if isinstance(call.caller, String):
            call.caller.reference = self.wrap_string(call.caller, to_lift)
        return to_lift + self.lift(call.args)

    def lift_print(self, call):
        return self.lift(call.args)

    def lift_return(self, ret):
        if isinstance(ret.value, String):
            to_lift = []
            ret.value.reference = self.wrap_string(ret.value, to_lift)
            return to_lift
        else:
            return self.lift(ret.value)

    def lift_program(self, program):
        to_lift = self.lift(program.statements)
        to_lift += self.lift(program.functions)
        program.literals = to_lift
        return program

    def lift_discard(self, discard):
        if isinstance(discard.expr, String):
            to_lift = []
            discard.expr.reference = self.wrap_string(discard.expr, to_lift)
            return to_lift
        else:
            return self.lift(discard.expr)

    def lift_list(self, node):
        to_lift = self.lift(node.elems)
        return to_lift

    def lift_statements(self, statements):
        to_lift = []
        for i in range(len(statements)):
            statement = statements[i]
            if isinstance(statement, String):
                statements[i].reference = self.wrap_string(statement, to_lift)

            else:
                to_lift += self.lift(statement)

        return to_lift

    def lift(self, node):
        if isinstance(node, list):
            return self.lift_statements(node)

        if not node is None:
            method_name = "lift_" + node.to_snake_case()
        else:
            return []

        if hasattr(self, method_name):
            return getattr(self, method_name)(node)
        else:
            return []


def lift_literals(program):
    str_lift = StringLifter()
    return str_lift.lift(program)
