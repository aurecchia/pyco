#!/usr/bin/env python
#

import ply.lex as lex
from nodes.AST import PyCoException

__author__ = "Ilya Yanok, Nate Nystrom"
__version__ = "Version 1.0, 10 Oct 2013"

states = (
    ('indent', 'exclusive'),
    ('dedent', 'exclusive'),
    ('comment', 'exclusive'),
    ('main', 'exclusive')
)

tokens = [
    'INDENT', 'DEDENT', 'IDENTIFIER', 'NEWLINE',
    'OPAREN', 'CPAREN', 'OBRACKET', 'CBRACKET',
    'STRING', 'INTEGER',
    "ASSIGN", "AUG_ASSIGN",
    "ADD", "SUB", "INV",
    "MUL", "DIV", "RSHIFT", "LSHIFT", "BITAND", "BITOR", "BITXOR", "FDIV",
    "MOD", "POW", "COMMA", "COLON", "DOT",
    "EQ", "NEQ", "NEQ2", "LT", "GT", "LEQ", "GEQ"
]



# Assignments
t_main_ASSIGN = r"="
t_main_AUG_ASSIGN = r"(\+|-|\*|/|//|%|\*\*|<<|>>|&|\||\^)="

# Binary and Unary operations
t_main_POW = r"\*\*"
t_main_ADD = r"\+"
t_main_SUB = r"-"
t_main_MUL = r"\*"
t_main_DIV = r"/"
t_main_RSHIFT = r">>"
t_main_LSHIFT = r"<<"
t_main_BITAND = r"&"
t_main_BITOR = r"\|"
t_main_BITXOR = r"\^"
t_main_INV = r"~"
t_main_FDIV = r"//"
t_main_MOD = r"%"
t_main_COMMA = r","
t_main_DOT = r"\."
t_main_COLON = r":"
t_main_EQ = r"=="
t_main_NEQ = r"!="
t_main_NEQ2 = r"<>"
t_main_LT = r"<"
t_main_GT = r">"
t_main_LEQ = r"<="
t_main_GEQ = r">="

# Constants
t_main_INTEGER = r"\d+"

# Keywords
reserved = {
    "print": "PRINT",
    "if": "IF",
    "else": "ELSE",
    "elif": "ELIF",
    "while": "WHILE",
    "True": "TRUE",
    "False": "FALSE",

    "and": "AND",
    "or": "OR",
    "not": "NOT",

    "def": "DEF",
    "lambda": "LAMBDA",
    "return": "RETURN",
    "None": "NONE",
}
tokens.extend(reserved.values())

t_indent_ignore = ''
t_dedent_ignore = ''


class PyCoIndentationError(PyCoException):

    def __repr__(self):
        return "Indentation error!"


def t_indent_error(t):
    raise PyCoIndentationError()


def t_dedent_error(t):
    raise PyCoIndentationError()


def t_comment_error(t):
    pass


def t_error(t):
    pass


def t_comment_skip(t):
    r'[^\n]+'
    pass


def t_ANY_NEWLINE(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
    if len(t.lexer.paren_stack) == 0:
        t.lexer.curr_indent = 0
        if t.lexer.lexstate != 'indent':
            t.lexer.begin('indent')
            return t


def t_main_softnewline(t):
    r'\\\n'
    t.lexer.lineno += 1


def t_indent_comment(t):
    r'\#'
    t.lexer.begin('comment')


def char_val(c):
    if c == ' ':
        return 1
    elif c == '\t':
        return 8
    else:
        return 0


def t_indent_ws(t):
    r'[ \t]+'
    t.lexer.curr_indent += sum(map(char_val, t.value))


def process_indent(lexer):
    cnt = 0
    curr = lexer.curr_indent
    topi = lexer.indents.pop()
    while topi > curr:
        topi = lexer.indents.pop()
        cnt += 1
    lexer.indents.append(topi)
    if topi == curr:
        return -cnt
    if topi < curr:
        if cnt == 0:
            return 1
        raise PyCoIndentationError()


def t_indent_INDENT(t):
    r'[^ \#\t\n]'
    t.lexer.lexpos -= 1
    val = process_indent(t.lexer)
    if val == 0:
        t.lexer.begin('main')
    elif val < 0:
        t.lexer.begin('dedent')
        t.lexer.dedent_cnt = -val
    else:
        t.lexer.indents.append(t.lexer.curr_indent)
        t.lexer.begin('main')
        return t


def t_dedent_DEDENT(t):
    r'.'
    t.lexer.lexpos -= 1
    t.lexer.dedent_cnt -= 1
    if t.lexer.dedent_cnt == 0:
        t.lexer.begin('main')
    return t


invert_paren = {
    '(': ')',
    '[': ']',
    '{': '}'
}

paren_token_name = {
    '(': 'OPAREN',
    ')': 'CPAREN',
    '[': 'OBRACKET',
    ']': 'CBRACKET',
    '{': 'OCURLY',
    '}': 'CCURLY'
}


def t_main_open(t):
    r'[\(\[\{]'
    t.lexer.paren_stack.append(invert_paren[t.value])
    t.type = paren_token_name[t.value]
    return t


def t_main_close(t):
    r'[\)\]\}]'
    last = t.lexer.paren_stack.pop()
    if last != t.value:
        print "Unmatched", t.value
        t.lexer.paren_stack.append(last)
    t.type = paren_token_name[t.value]
    return t


def token_override(self):
    t = self.token_()
    if t is None:
        return process_eof(self)
    return t


def process_eof(lexer):
    if len(lexer.indents) == 1:
        return None
    lexer.indents.pop()

    tok = lex.LexToken()
    tok.value = ''
    tok.type = 'DEDENT'
    tok.lineno = lexer.lineno
    tok.lexpos = lexer.lexpos
    tok.lexer = lexer
    return tok

###############


def process_string(string):
    return string


def t_main_string_short_single(t):
    r"(r|R|UR|Ur|U|uR|ur|u|br|bR|b|BR|Br|B)?'[^'\n]*'"
    t.type = 'STRING'
    return process_string(t)


def t_main_string_short_double(t):
    r'(r|R|UR|Ur|U|uR|ur|u|br|bR|b|BR|Br|B)?"[^"\n]*"'
    t.type = 'STRING'
    return process_string(t)


literals = ".@,:`;~"

t_main_ignore = ' \t'


def t_main_IDENTIFIER(t):
    r'[a-zA-Z_]\w*'
    # tries to get the received input from the reserved python keywords
    # if there is no keyword with the corresponding name returns an identifier
    t.type = reserved.get(t.value, "IDENTIFIER")
    return t


def t_main_error(t):
    pass


def t_main_comment(t):
    '\#'
    t.lexer.begin('comment')


def init_lexer():
    lexer = lex.lex()
    lexer.indents = []
    lexer.indents.append(0)
    lexer.paren_stack = []
    lexer.curr_indent = 0
    lexer.token_ = lexer.token
    lexer.token = (lambda: token_override(lexer))
    lexer.begin('indent')

    # Uncomment to see tokens
    #lex.runmain(lexer)
