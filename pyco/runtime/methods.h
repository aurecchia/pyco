﻿#ifndef _PYCO_METHODS
#define _PYCO_METHODS


/** Dispatches the call to \b index depending on the type of the iterable. */
long index(const long iter, const long elem);


/** Dispatches he call to \b count depending on the type of the iterable. */
long count(const long iter, const long elem);


/** Dispatches the call to \b get depending ton the type the iterable. */
long get(const long iter, const long index);


/** Dispatches the call to \b length depending ton the type the iterable. */
long length(const long iter);


/** Dispatches the call to \b slice depending ton the type the iterable. */
long slice(const long iter, const long from, const long to);


/** Dispatches the call to \b slice depending ton the type the iterable. */
long slice_up_to(const long iter, const long to);


/** Dispatches the call to \b slice depending ton the type the iterable. */
long slice_from(const long iter, const long from);

#endif

