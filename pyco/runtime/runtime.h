﻿#ifndef _PYCO_RUNTIME_H
#define _PYCO_RUNTIME_H

/** Reads and returns a value from stdin.
 *
 * After having read the value it tags it with its correct type, if the read
 * value is not supported by the language, then it raises an error.
 */
long input();


/** Function called when trying to print an unprintable value. */
void unprintable_value();


/** Function called when an invalid value is received from input. */
void invalid_input_error();


/** Utility function used to dispatch the print for the correct type. */
void _dispatch_print(long value);


/** Print to stdout without adding anything. */
void prompt(long value);


/** Print to stdout. */
void print(long value);


/** Print to stdout with new-line */
void print_nl( long value );


/** Print an empty line to stdout. */
void print_empty();


#endif
