﻿#include <stdio.h>
#include <stdlib.h>

#include "tags.h"


void undefined_tag_error(const int tag)
{
  fprintf(stderr, "[!] Undefined type error, got: %d \n", tag);
  exit(1);
}


void wrong_tag_error()
{
  fprintf(stderr, "[!] Received value with incorrect expected tag.");
  exit(1);
}


void check_tag(int tag)
{
    if ( tag < 0 || tag > 4 )
          undefined_tag_error( tag );
}


int get_tag(long value)
{
    int tag = value & TAG_MASK;
    check_tag( tag );
    return tag;
}


const char * get_tag_name(int tag)
{
    switch( tag )
    {
        case int_tag:
            return "integer";
        case bool_tag:
            return "boolean";
        case str_tag:
            return "string";
        case ref_tag:
            return "reference";
        case list_tag:
            return "list";
    }
}


int has_tag(long value, tag_t type)
{
    return get_tag( value ) == type;
}


int is_pointer_tag(int tag)
{
    switch ( tag )
    {
        case int_tag:
        case bool_tag:
          return 0;
        case str_tag:
        case ref_tag:
        case list_tag:
          return 1;
        default:
          undefined_tag_error( tag );
    }
}


int is_primitive_tag(int tag)
{
    return !is_pointer_tag( tag );
}


long tag(long value, tag_t tag)
{
    if ( is_pointer_tag(tag) )
      return value + tag;
    else
      return ( value << TAG_SIZE ) + tag;
}


long tag_result(long value, long left, long right)
{
  return ( value << TAG_SIZE ) | ( get_tag( left ) & get_tag( right ) );
}


long untag(long value)
{
    if ( is_pointer_tag(get_tag(value)) )
        return value & VALUE_MASK;
    else
        return value >> TAG_SIZE;
}


long untag_check(const long value, const tag_t tag)
{
    if ( !has_tag( value, tag ) )
        wrong_tag_error();
    return untag( value );
}



int get_tag_value(const long tag)
{
    switch( tag )
    {
        case ref_tag:
            return 0;
        case bool_tag:
            return 1;
        case int_tag:
            return 2;
        case list_tag:
            return 3;
        case str_tag:
            return 4;
        default:
          undefined_tag_error( tag );
    }
}


