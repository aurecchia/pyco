﻿#include <stdio.h>

#include "primitives.h"
#include "tags.h"


long primitives_add(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    return tag( left_val + right_val, int_tag );
}


long primitives_uadd(const long value)
{
    const long untagged = untag( value );

    return tag( untagged, int_tag );
}


long primitives_sub(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    return tag( left_val - right_val, int_tag );
}


long primitives_usub(const long value)
{
    const long untagged = untag( value );

    return tag( -untagged, int_tag );
}


long primitives_mul(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    return tag( left_val * right_val, int_tag );
}


long primitives_div(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    return tag( left_val / right_val, int_tag );
}


long primitives_mod(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    return tag( left_val % right_val, int_tag );
}


long primitives_bitor(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    if ( get_tag( left ) == int_tag || get_tag( right ) == int_tag )
        return tag( left_val | right_val, int_tag );

    return tag( left_val | right_val, bool_tag );
}



long primitives_bitand(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    if ( get_tag( left ) == int_tag || get_tag( right ) == int_tag )
        return tag( left_val & right_val, int_tag );

    return tag( left_val & right_val, bool_tag );
}


long primitives_bitxor(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    if ( get_tag( left ) == int_tag || get_tag( right ) == int_tag )
        return tag( left_val ^ right_val, int_tag );

    return tag( left_val ^ right_val, bool_tag );
}


long primitives_bitnot(const long left)
{
    const long left_val = untag( left );

    return tag( ~ left_val, int_tag );
}


long primitives_left_shift(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    return tag( left_val << right_val, int_tag );
}



long primitives_right_shift(const long left, const long right)
{
    const long left_val = untag( left );
    const long right_val = untag( right );

    return tag( left_val >> right_val, int_tag );
}


long primitives_or(const long left, const long right)
{
    if ( untag( left ) == 0 )
        return right;

    return left;
}


long primitives_and(const long left, const long right)
{
    if ( untag( left ) == 0 )
        return left;

    return right;
}


long primitives_not(const long left)
{
    const long left_val = untag( left );

    return tag( !left_val, bool_tag );
}


long primitives_compare(const long first, const long second)
{
    const long first_val = untag( first );
    const long second_val = untag( second );

    if ( first_val < second_val )
        return -1;

    else if ( first_val == second_val )
        return 0;

    else
        return 1;
}


void int_print(const long value)
{
    printf("%ld", untag_check( value, int_tag ));
}


void bool_print(const long value)
{
    const long boolean = untag_check( value, bool_tag );

    printf( boolean ? "True" : "False" );
}


