#include <stdio.h>
#include <string.h>
#include <gc.h>

#define TABLE_L 20
#define STRING_MAX 256


/** A pair of key and value (used internally in the hashtable). */
typedef struct pair
{
  /** The id/key of the pair. */
  char * id;
  /** The value of the pair. */
  long value;
  /** The pointer to the next pair in case of collisions. */
  struct pair * next;
} pair_t;


/** A hashtable containing elements of type <pair_t>. */
typedef struct
{
  /** An array of pointers to the contained pairs. */
  pair_t ** elems;
  /** The size of the table. */
  unsigned long size;
} hashtable_t;


/**
 * Return a hash value for strings.
 *
 * @param string The string to hash.
 * @param mod The string to hash.
 * @return The hash value.
 */
unsigned long hash(const char * string, unsigned long mod);


/**
 * Creates a new hashtable and returns it.
 *
 * @return The newly created hashtable.
 */
hashtable_t * new_table(unsigned long size);


/**
 * Creates a new key/value pair.
 *
 * @param id The key, which will be used as identifier.
 * @param value The value.
 * @return The newly created pair.
 */
pair_t * _new_pair(const char * id, const long value);


/**
 * Puts the key/value pair in the hashtable.
 *
 * Puts the key/value pair in the hashtable taking care of eventually updating
 * already existing pairs.
 * If a pair with the same key is already stored in the table, then its value
 * is updated with the new one, otherwise a new pair is inserted. In case there
 * are conflicts with other pairs with different keys, then the new pair is
 * inserted at the beginning of the list.
 *
 * @param table The table in which to put the pair.
 * @param key The key of the pair.
 * @param value The value of the pair.
 */
void hashtable_put(hashtable_t * table, const char * key, const long value);


/**
 * brief Gets a value stored in the table.
 *
 * Retrieves a value stored in the table with the given key taking care of
 * non-existing values and conflicts.
 * If the given key is not in the three, then it returns NULL.
 *
 * @param table The table in which to search for the value.
 * @param key The key to use for the lookup.
 * @return The value if the key was stored, NULL otherwise.
 */
long hashtable_get(hashtable_t * table, const char * key);

