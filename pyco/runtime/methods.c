﻿#include "methods.h"

#include <stdio.h>
#include <stdlib.h>

#include "tags.h"
#include "strings.h"
#include "list.h"


void unsupported_method_error(const char * method)
{
    fprintf( stderr, "[!] The given value does not have method %s.", method);
    exit(-1);
}

long index(const long iter, const long elem)
{
    const int tag = get_tag( iter );

    switch ( tag )
    {
        case str_tag:
            return string_index( iter, elem );
        case list_tag:
            return list_index( iter, elem );
        default:
            unsupported_method_error( "append" );
    }
}


long count(const long iter, const long elem)
{
    const int tag = get_tag( iter );

    switch ( tag )
    {
        case str_tag:
            return string_count( iter, elem );
        case list_tag:
            return list_count( iter, elem );
        default:
            unsupported_method_error( "count" );
    }
}


long get(const long iter, const long index)
{
    const int tag = get_tag( iter );

    switch ( tag )
    {
        case str_tag:
            return string_get( iter, index );
        case list_tag:
            return list_get( iter, index );
        default:
            unsupported_method_error( "get" );
    }
}


long length(const long iter)
{
    const int tag = get_tag( iter );

    switch ( tag )
    {
        case str_tag:
            return string_len( iter );
        case list_tag:
            return list_len( iter );
        default:
            unsupported_method_error( "len" );
    }
}


long slice(const long iter, const long from, const long to)
{
    const int tag = get_tag( iter );

    switch ( tag )
    {
        case str_tag:
            return string_slice( iter, from, to );
        case list_tag:
            return list_slice( iter, from, to );
        default:
            unsupported_method_error( "slice" );
    }
}


long slice_up_to(const long iter, const long to)
{
    const int tag = get_tag( iter );

    switch ( tag )
    {
        case str_tag:
            return string_slice_up_to( iter, to );
        case list_tag:
            return list_slice_up_to( iter, to );
        default:
            unsupported_method_error( "slice" );
    }
}


long slice_from(const long iter, const long from)
{
    const int tag = get_tag( iter );

    switch ( tag )
    {
        case str_tag:
            return string_slice_from( iter, from );
        case list_tag:
            return list_slice_from( iter, from );
        default:
            unsupported_method_error( "slice" );
    }
}

