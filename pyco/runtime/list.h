﻿#ifndef _PYCO_LIST_H
#define _PYCO_LIST_H


#define DEFAULT_LEN 128

typedef struct list
{
    long *elems;
    int size;
    int max;
} list_t;


/** Utility function to create a lsit of the given length. */
list_t * _list_new(const int length);


/** Utility function for enlarging lists. (Doubles the current max size). */
void _list_enlarge(list_t * list_ptr);


/** Utility function for shrinking lists.
 *
 * Halves the size of the list (if possible). The minimum size is the default
 * starting length of all lists.
 */
void _list_shrink(list_t * list_ptr);


/** Default "constructor" for a list. (initialized with the default length). */
long list_new_default();


/** "Constructor" for a list of the given length. */
long list_new(const int length);


/** Returns the length of the list. */
long list_len(const long list);


/** Appends the given element to a list. */
void list_append(const long list, const long elem);


/** Extends a list with all the elements of another list. */
void list_extend(const long dest, const long source);


/** Inserts an element into a list at the given position. */
void list_insert(const long list, const long index, const long elem);


/** Removes the given element from the list. */
void list_remove(const long list, const long elem);


/** Pops and returns the last element from the list. */
long list_pop_last(const long list);


/** Pops and returns an element in the list at the given position. */
long list_pop(const long list, const long index);


/** Returns the index of the given element in the list. */
long list_index(const long list, const long elem);


/** Counts the occurrences of the element in the list. */
long list_count(const long list, const long elem);


/** Reverses the order of the list in place. */
void list_reverse(const long list);


/** Prints the contents of the list. */
void list_print(const long list);


/** Returns the elements in the list at the given index. */
long list_get(const long list, const long index);


/** Puts an element in the list at the given index.
 *
 * WARNING: it does not work as list insert: if another element is at the given
 * index, then it is overwritten and the new given one is put there.
 */
void list_put(const long list, const long index, const long elem);


/** Returns a slice of a given list as a new list. */
long list_slice(const long list, const long from, const long to);


/** Returns a slice of a given list, from the beginning, as a new list. */
long list_slice_up_to(const long list, const long to);


/** Returns a slice of a given list, until the end, as a new list. */
long list_slice_from(const long list, const long from);


/** Substitutes a slice of a list with another list. */
void list_put_list(const long dest, const long dest_from, const long dest_to,
        const long source);

/** Performs the addition of the two lists and returns a new one. */
long list_add(const long first, const long second);


/** Performs the multiplication of one list and returns a new one. */
long list_mul(const long list, const long factor);


/** Comparator for strings. */
long list_compare(const long first, const long second);


#endif // _PYCO_LIST_H
