﻿#include <stdio.h>
#include <stdlib.h>
#include <gc.h>
#include <math.h>

#include "tags.h"
#include "list.h"
#include "operations.h"
#include "runtime.h"


void pop_from_empty_list_error()
{
    fprintf( stderr, "[!] Pop from empty list!");
    exit(-1);
}


void pop_index_out_of_range_error()
{
    fprintf( stderr, "[!] Pop index out of range!");
    exit(-1);
}


void item_not_in_list_error(const long item)
{
    fprintf( stderr, "[!] Item <%ld> not in list!", item >> 3);
    exit(-1);
}


void list_non_primitive_mul_error(const long item)
{
    const char * form = "[!] Multiplying list with non-primitive value: %ld";
    fprintf( stderr, form, item >> 3);
    exit(-1);
}


void list_non_primitive_index_error()
{
    fprintf( stderr, "[!] List indices must be primitives.");
    exit(-1);
}


list_t * _list_new(const int length)
{
    const size_t struct_size = sizeof(list_t);
    list_t * list = (list_t *) GC_MALLOC( struct_size );

    const int len = (int) pow( 2, ceil( log2( length ) ) );
    list->elems = (long *) GC_MALLOC( sizeof(long) * len );
    list->size = 0;
    list->max = length;

    return list;
}


long list_new_default()
{
    return tag( (long) _list_new( DEFAULT_LEN ), list_tag );
}


long list_new(const int length)
{
    return tag( (long) _list_new( length ), list_tag );
}


void _list_enlarge(list_t * list_ptr)
{
    const int len = 2 * list_ptr->max;

    long * new_elems = (long *) GC_MALLOC( sizeof(long) * len );

    for (int i = 0; i < list_ptr->size; ++i)
        new_elems[i] = list_ptr->elems[i];

    list_ptr->elems = new_elems;
    list_ptr->max = len;
}

void _list_shrink(list_t * list_ptr)
{
    const int len = list_ptr->max / 2;

    long * new_elems = (long *) GC_MALLOC( sizeof(long) * len );

    for (int i = 0; i < list_ptr->size; ++i)
        new_elems[i] = list_ptr->elems[i];

    list_ptr->elems = new_elems;
    list_ptr->max = len;
}


long list_len(const long list)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    return tag( list_ptr->size, int_tag );
}


void list_append(const long list, const long elem)
{
    list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    if ( list_ptr->size == list_ptr-> max )
        _list_enlarge(list_ptr);

    list_ptr->elems[list_ptr->size] = elem;
    list_ptr->size++;
}


void list_extend(const long dest, const long source)
{
    list_t * dest_ptr = (list_t *) untag_check( dest, list_tag );
    const list_t * source_ptr = (list_t *) untag_check( source, list_tag );

    while ( dest_ptr->max - dest_ptr->size < source_ptr->size )
        _list_enlarge( dest_ptr );

    for ( int i = 0; i < source_ptr->size; ++i )
        dest_ptr->elems[dest_ptr->size + i] = source_ptr->elems[i];

    dest_ptr->size += source_ptr->size;
}


void list_insert(const long list, const long index, const long elem)
{
    list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    if ( !is_primitive_tag(get_tag(index)) )
        list_non_primitive_index_error();

    int pos = untag( index );
    if ( pos <= -list_ptr->size )
        pos = 0;
    else if ( pos < 0 )
        pos += list_ptr->size;

    if ( pos >= list_ptr->size )
    {
        list_append( list, elem );
        return;
    }

    if ( list_ptr->size == list_ptr-> max )
        _list_enlarge(list_ptr);


    for ( int i = list_ptr->size - 1; i >= pos; --i )
    {
        list_ptr->elems[i + 1] = list_ptr->elems[i];
    }

    list_ptr->elems[pos] = elem;
    list_ptr->size++;
}


void list_remove(const long list, const long elem)
{
    list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    int found = 0;
    for ( int i = 0; i < list_ptr->size; ++i )
    {
        if ( found )
            list_ptr->elems[i - 1] = list_ptr->elems[i];
        else
            found = ( compare(list_ptr->elems[i], elem) == 0 );
    }
    list_ptr->size--;

    const int len = (int) pow( 2, ceil( log2( list_ptr->size ) ) );
    if ( len < list_ptr->max )
        _list_shrink( list_ptr );

    if ( !found )
        item_not_in_list_error(elem);
}


long list_pop_last(const long list)
{
    list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    if ( list_ptr->size == 0 )
        pop_from_empty_list_error();

    list_ptr->size--;

    const long ret = list_ptr->elems[list_ptr->size];

    const int len = (int) pow( 2, ceil( log2( list_ptr->size ) ) );
    if ( len < list_ptr->max )
        _list_shrink( list_ptr );

    return ret;
}


long list_pop(const long list, const long index)
{
    list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    if ( list_ptr->size == 0 )
        pop_from_empty_list_error();

    if ( !is_primitive_tag(get_tag(index)) )
        list_non_primitive_index_error();

    int pos = untag( index );
    if ( pos < -list_ptr->size || pos >= list_ptr->size )
        pop_index_out_of_range_error();
    if ( pos < 0 )
        pos += list_ptr->size;

    const long ret = list_ptr->elems[pos];

    for ( int i = pos + 1; i < list_ptr->size; ++i )
        list_ptr->elems[i - 1] = list_ptr->elems[i];

    list_ptr->size--;

    const int len = (int) pow( 2, ceil( log2( list_ptr->size ) ) );
    if ( len < list_ptr->max )
        _list_shrink( list_ptr );

    return ret;
}


long list_index(const long list, const long elem)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    for ( int i = 0; i < list_ptr->size; ++i )
    {
        if ( compare(list_ptr->elems[i], elem) == 0 )
            return tag ( i, int_tag );
    }

    item_not_in_list_error(elem);
}


long list_count(const long list, const long elem)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    long count = 0;
    for ( int i = 0; i < list_ptr->size; ++i )
    {
        if ( compare(list_ptr->elems[i], elem) == 0 )
            count++;
    }

    return tag( count, int_tag );
}


void list_reverse(const long list)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    for ( int i = 0; i < list_ptr->size / 2; ++i )
    {
        const long temp = list_ptr->elems[i];
        list_ptr->elems[i] = list_ptr->elems[list_ptr->size - 1 - i];
        list_ptr->elems[list_ptr->size - 1 - i] = temp;
    }
}


long list_get(const long list, const long index)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    if ( !is_primitive_tag(get_tag(index)) )
        list_non_primitive_index_error();

    int pos = untag( index );
    if ( pos < -list_ptr->size || pos >= list_ptr->size )
        pop_index_out_of_range_error();

    if ( pos < 0 )
        pos += list_ptr->size;

    return list_ptr->elems[pos];
}


void list_put(const long list, const long index, const long elem)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    if ( !is_primitive_tag(get_tag(index)) )
        list_non_primitive_index_error();

    int pos = untag( index );
    if ( pos < -list_ptr->size || pos >= list_ptr->size )
        pop_index_out_of_range_error();

    if ( pos < 0 )
        pos += list_ptr->size;

    list_ptr->elems[pos] = elem;
}


long list_slice(const long list, const long from, const long to)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    if ( !is_primitive_tag(get_tag(from)) )
        list_non_primitive_index_error();

    if ( !is_primitive_tag(get_tag(to)) )
        list_non_primitive_index_error();

    int bottom = untag( from );
    if ( bottom < 0 && bottom > -list_ptr->size )
        bottom += list_ptr->size;
    else if ( bottom < -list_ptr->size )
        bottom = 0;
    else if ( bottom > list_ptr->size && list_ptr->size > 0 )
        bottom = list_ptr->size;

    int top = untag( to );
    if ( top < 0 && top > -list_ptr->size )
        top += list_ptr->size;
    else if ( top < -list_ptr->size )
        top = 0;
    else if ( top > list_ptr->size && list_ptr->size > 0 )
        top = list_ptr->size;

    int size = top - bottom;
    if ( size < 0 )
        size = 0;

    list_t * ret = _list_new( size );
    ret->size = size;

    for ( int i = bottom; i < top; ++i )
        ret->elems[i - bottom] = list_ptr->elems[i];

    return tag( (long) ret, list_tag );
}


long list_slice_up_to(const long list, const long to)
{
    const long from = tag( 0, int_tag );

    return list_slice( list, from, to );
}


long list_slice_from(const long list, const long from)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );
    const long to = tag( list_ptr->size, int_tag );

    return list_slice( list, from, to );
}


void list_put_list(const long dest, const long from, const long to,
        const long source)
{
    list_t * dest_ptr = (list_t *) untag_check( dest, list_tag );
    const list_t * source_ptr = (list_t *) untag_check( source, list_tag );

    if ( !is_primitive_tag(get_tag(from)) )
        list_non_primitive_index_error();

    if ( !is_primitive_tag(get_tag(to)) )
        list_non_primitive_index_error();

    int bottom = untag( from );
    if ( bottom < 0 && bottom > -dest_ptr->size )
        bottom += dest_ptr->size;
    else if ( bottom < -dest_ptr->size )
        bottom = 0;
    else if ( bottom > dest_ptr->size && dest_ptr->size > 0 )
        bottom = dest_ptr->size;

    int top = untag( to );
    if ( top < 0 && top > -dest_ptr->size )
        top += dest_ptr->size;
    else if ( top < -dest_ptr->size )
        top = 0;
    else if ( top > dest_ptr->size && dest_ptr->size > 0 )
        top = dest_ptr->size;

    int size = top - bottom;
    if ( size < 0 )
        size = 0;

    int index = tag( bottom, int_tag );

    while ( size-- > 0 )
        list_pop( dest, index );

    for ( int i = 0; i < source_ptr->size; ++i )
    {
        index = tag( bottom + i, int_tag );
        list_insert( dest, index, source_ptr->elems[i] );
    }
}


long list_add(const long first, const long second)
{
    const list_t * first_ptr = (list_t *) untag_check( first, list_tag );
    const list_t * second_ptr = (list_t *) untag_check( second, list_tag );

    list_t * list_ptr = _list_new( first_ptr->size + second_ptr->size );
    long list = tag( (long) list_ptr, list_tag );

    list_extend( list, first );
    list_extend( list, second );

    return list;
}


long list_mul(const long list, const long factor)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    if ( !is_primitive_tag( get_tag(factor) ) )
        list_non_primitive_mul_error( factor );

    long times = untag( factor );

    if ( times < 0 )
        times = 0;

    list_t * new_list_ptr = _list_new( list_ptr->size * times );
    const long new_list = tag( (long) new_list_ptr, list_tag );

    while ( times-- > 0 )
        list_extend( new_list, list );

    return new_list;
}


long list_compare(const long first, const long second)
{
    const list_t * first_ptr = (list_t *) untag_check( first, list_tag );
    const list_t * second_ptr = (list_t *) untag_check( second, list_tag );

    for ( int i = 0; i < first_ptr->size; ++i )
    {
        if ( i >= second_ptr->size)
            return 1;

        const long first_elem = first_ptr->elems[i];
        const long second_elem = second_ptr->elems[i];

        const long res = compare( first_elem, second_elem );

        if ( res != 0 )
            return res;
    }

    if ( first_ptr->size < second_ptr->size )
        return 1;

    return 0;
}


void list_print(const long list)
{
    const list_t * list_ptr = (list_t *) untag_check( list, list_tag );

    printf("[");
    for ( int i = 0; i < list_ptr->size; ++i )
    {
        if ( i > 0 )
            printf(", ");

        if ( has_tag( list_ptr->elems[i], str_tag ) )
        {
            printf("'");
            _dispatch_print( list_ptr->elems[i] );
            printf("'");
        }
        else
        {
            _dispatch_print( list_ptr->elems[i] );
        }

    }
    printf("]");
}


