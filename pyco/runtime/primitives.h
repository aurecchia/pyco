﻿#ifndef _PYCO_PRIMITIVES_H
#define _PYCO_PRIMITIVES_H


/** Returns the addition of the two values. */
long primitives_add(const long left, const long right);


/** Returns the addition of the two values. */
long primitives_uadd(const long value);


/** Returns the difference of the two values. */
long primitives_sub(const long left, const long right);


/** Returns the difference of the two values. */
long primitives_usub(const long value);


/** Returns the multiplication of the two values. */
long primitives_mul(const long left, const long right);


/** Returns the division between the two values. */
long primitives_div(const long left, const long right);


/** Returns the remainder of the modulo between the two values. */
long primitives_mod(const long left, const long right);


/** Returns the bitwise or between the two values. */
long primitives_bitor(const long left, const long right);


/** Returns the bitwise and between the two values. */
long primitives_bitand(const long left, const long right);


/** Returns the bitwise not of the value. */
long primitives_bitnot(const long left);


/** Returns the bitwise xor between the two values. */
long primitives_bitxor(const long left, const long right);


/** returns \b left shifted left by \b right bits. */
long primitives_left_shift(const long left, const long right);


/** returns \b left shifted right by \b right bits. */
long primitives_right_shift(const long left, const long right);


/** Returns the logical and between the two values. */
long primitives_and(const long left, const long right);


/** Returns the logical or between the two values. */
long primitives_or(const long left, const long right);


/** Returns the logical not of the value. */
long primitives_not(const long left);


/** Prints an int to standard output. */
/** Comparator for primitives. */
long primitives_compare(const long first, const long second);


/** Prints an int to stdout. */
void int_print(const long value);


/** Prints a boolean to stdout. */
void bool_print(const long value);


#endif
