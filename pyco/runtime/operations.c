﻿#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "operations.h"
#include "tags.h"
#include "primitives.h"
#include "list.h"
#include "strings.h"


void unsupported_operation_error(const char * op, const long left,
        const long right)
{
    const char * left_tag_string = get_tag_name(get_tag(left));
    const char * right_tag_string = get_tag_name(get_tag(right));

    fprintf( stderr, "[!] Operation <%s> is unsupported for types %s and %s.",
            op, left_tag_string, right_tag_string);
    exit(-1);
}


void unsupported_unary_operation_error(const char * op, const long left)
{
    const char * left_tag_string = get_tag_name(get_tag(left));

    fprintf( stderr, "[!] Operation <%s> is unsupported for type %s.",
            op, left_tag_string);
    exit(-1);
}


void not_compatible_error()
{
    fprintf( stderr, "[!] The two received values are not compatible.");
    exit(-1);
}


void not_comparable_error()
{
    fprintf( stderr, "[!] The two received values cannot be compared.");
    exit(-1);
}


long add(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_add( left, right );

    if ( has_tag(left, list_tag) && has_tag(right, list_tag) )
        return list_add( left, right );

    if ( has_tag(left, str_tag) && has_tag(right, str_tag) )
        return string_add( left, right );

    unsupported_operation_error("+", left, right);
}


long uadd(const long value)
{
    if ( !is_primitive_tag(get_tag(value)) )
        unsupported_unary_operation_error("+", value);

    return primitives_uadd( value );
}


long sub(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_sub( left, right );

    unsupported_operation_error("-", left, right);
}


long usub(const long value)
{
    if ( !is_primitive_tag(get_tag( value )) )
        unsupported_unary_operation_error("-", value);

    return primitives_usub( value );
}


long mul(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_mul( left, right );

    if ( has_tag(left, list_tag) && is_primitive_tag(right_tag) )
        return list_mul( left, right );

    if ( is_primitive_tag(left_tag) && has_tag(right, list_tag) )
        return list_mul( right, left );

    if ( has_tag(left, str_tag) && is_primitive_tag(right_tag) )
        return string_mul( left, right );

    if ( is_primitive_tag(left_tag) && has_tag(right, str_tag) )
        return string_mul( right, left );

    unsupported_operation_error("*", left, right);
}


long intdiv(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_div( left, right );

    unsupported_operation_error("/", left, right);
}


long floordiv(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_div( left, right );

    unsupported_operation_error("//", left, right);
}



long power(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return (long) ( pow( (double) left, (double) right ) );

    unsupported_operation_error("**", left, right);
}



long mod(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_mod( left, right );

    unsupported_operation_error("%", left, right);
}




long bitor(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_bitor( left, right );

    unsupported_operation_error("|", left, right);
}



long bitand(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_bitand( left, right );

    unsupported_operation_error("&", left, right);
}


long bitxor(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_bitxor( left, right );

    unsupported_operation_error("^", left, right);
}

long bitnot(const long left)
{
    const int left_tag = get_tag( left );

    if ( is_primitive_tag(left_tag)  )
        return primitives_bitnot( left );

    unsupported_unary_operation_error("~", left);
}


long left_shift(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_left_shift( left, right );

    unsupported_operation_error("<<", left, right);
}


long right_shift(const long left, const long right)
{
    const int left_tag = get_tag( left );
    const int right_tag = get_tag( right );

    if ( is_primitive_tag(left_tag) && is_primitive_tag(right_tag) )
        return primitives_right_shift( left, right );

    unsupported_operation_error(">>", left, right);
}


long or(const long left, const long right)
{
    if ( untag( left ) == 0 )
        return right;

    return left;
}


long and(const long left, const long right)
{
    if ( untag( left ) == 0 )
        return left;

    return right;
}


long not(const long value)
{
    if ( untag( value ) == 0 )
        return tag( 1, bool_tag );

    return tag( 0, bool_tag );
}


long compare(const long first, const long second)
{
    const int first_tag = get_tag( first );
    const int second_tag = get_tag( second );

    if (
            first_tag != second_tag &&
            !( is_primitive_tag(first_tag) && is_primitive_tag(second_tag) )
        )
        not_compatible_error();

    switch ( first_tag )
    {
        case int_tag:
        case bool_tag:
            return primitives_compare( first, second );
        case str_tag:
            return string_compare( first, second );
        case list_tag:
            return list_compare( first, second );
        case ref_tag:
            {
            const long first_val = untag( first );
            const long second_val = untag( second );

            if ( first_val < second_val )
                return -1;

            else if ( first_val == second_val )
                return 0;

            else
                return 1;
            }
        default:
            not_comparable_error();
    }
}

long eq(const long first, const long second)
{
    const int first_tag = get_tag( first );
    const int second_tag = get_tag( second );

    if ( (is_primitive_tag(first_tag) && is_primitive_tag(second_tag)) ||
            first_tag == second_tag )
        return tag( (compare(first, second) == 0), bool_tag );

    if ( ( first_tag == ref_tag && untag( first ) == 0 ) ||
            ( second_tag == ref_tag && untag( second ) == 0 ) )
        return tag( 0, bool_tag );

    return tag( untag(first) == untag(second), bool_tag );
}


long neq(const long first, const long second)
{
    const int first_tag = get_tag( first );
    const int second_tag = get_tag( second );

    if ( (is_primitive_tag(first_tag) && is_primitive_tag(second_tag)) ||
            first_tag == second_tag )
        return tag( (compare(first, second) != 0), bool_tag );

    if ( (first_tag == ref_tag && untag( first ) == 0) ||
            (second_tag == ref_tag && untag( second ) == 0) )
        return tag( 1, bool_tag );

    return tag( untag(first) != untag(second), bool_tag );
}


long lt(const long first, const long second)
{
    const int first_tag = get_tag( first );
    const int second_tag = get_tag( second );

    if ( (is_primitive_tag(first_tag) && is_primitive_tag(second_tag)) ||
            first_tag == second_tag )
        return tag( (compare(first, second) < 0), bool_tag );

    const int ret = get_tag_value( first_tag ) < get_tag_value( second_tag );
    return tag( ret, bool_tag );
}


long lte(const long first, const long second)
{
    const int first_tag = get_tag( first );
    const int second_tag = get_tag( second );

    if ( (is_primitive_tag(first_tag) && is_primitive_tag(second_tag)) ||
            first_tag == second_tag )
        return tag( (compare(first, second) <= 0), bool_tag );

    const int ret = get_tag_value( first_tag ) < get_tag_value( second_tag );
    return tag( ret, bool_tag );
}


long gt(const long first, const long second)
{
    const int first_tag = get_tag( first );
    const int second_tag = get_tag( second );

    if ( (is_primitive_tag(first_tag) && is_primitive_tag(second_tag)) ||
            first_tag == second_tag )
        return tag( (compare(first, second) > 0), bool_tag );

    const int ret = get_tag_value( first_tag ) > get_tag_value( second_tag );
    return tag( ret, bool_tag );
}


long gte(const long first, const long second)
{
    const int first_tag = get_tag( first );
    const int second_tag = get_tag( second );

    if ( (is_primitive_tag(first_tag) && is_primitive_tag(second_tag)) ||
            first_tag == second_tag )
        return tag( (compare(first, second) >= 0), bool_tag );

    const int ret = get_tag_value( first_tag ) > get_tag_value( second_tag );
    return tag( ret, bool_tag );
}


