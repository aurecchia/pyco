﻿#ifndef _PYCO_CLOSURE_H
#define _PYCO_CLOSURE_H

#include "hashtable.h"


/** Structure for function closure. */
typedef struct {
    void * function_p;
    hashtable_t * env;
} closure_t;


/** Creates the closure containing the given function pointer. */
long make_closure( void * function_pointer );


/** Puts the variable in the environment of the given closure. */
void put_env( long closure, char * key, long val );


/** Gets the value of the variable in the environment of the closure. */
long get_env( long closure, char * key );


#endif
