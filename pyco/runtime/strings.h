#ifndef _PYCO_STRINGS_H
#define _PYCO_STRINGS_H


/** Returns a new C string from the llvm one. */
long string_new(const void * string);


/** Returns the length of the string. */
long string_len(const long string);


/** Returns character at position index. */
long string_get(const long string, const long index);


/** Returns the index of the given element in the string. */
long string_index(const long string, const long elem);


/** Counts the occurrences of the element in the list. */
long string_count(const long string, const long elem);


/** Returns a slice of the string from \b from to \b to. */
long string_slice(const long string, const long from, const long to);


/** Returns a slice of a given string, from the beginning, as a new string. */
long string_slice_up_to(const long string, const long to);


/** Returns a slice of a given string, until the end, as a new string. */
long string_slice_from(const long string, const long from);


/** Returns the concatenation of the two given strings. */
long string_add(const long first, const long second);


/** Returns a string composed as \b factor times the given string. */
long string_mul(const long string, const long factor);


/** Comparator for strings. */
long string_compare(const long first, const long second);


/** Print the string to stdout. */
void string_print(const long string);


#endif // _PYCO_STRINGS_H
