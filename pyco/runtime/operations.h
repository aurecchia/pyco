#ifndef _PYCO_OPERATIONS_H
#define _PYCO_OPERATIONS_H


/** Returns the addition of the two values based on the types. */
long add(const long left, const long right);


/** Returns the unary addition of the value. */
long uadd(const long value);


/** Returns the subtraction of the two values. */
long sub(const long left, const long right);


/** Returns the unary subtraction of the value. */
long usub(const long value);


/** Returns the multiplication of the two values based on the types. */
long mul(const long left, const long right);


/** Returns the  division of the two values. */
long intdiv(const long left, const long right);


/** Returns the integer division of the two values. */
long floordiv(const long left, const long right);


/** Return the exponentiation of the values. */
long power(const long left, const long right);


/** Returns the bitwise or between the two values. */
long bitor(const long left, const long right);


/** Returns the bitwise and between the two values. */
long bitand(const long left, const long right);


/** Returns the bitwise not of the value. */
long bitnot(const long left);


/** Returns the bitwise xor between the two values. */
long bitxor(const long left, const long right);


/** Returns the \b left shifted left by \b right bits. */
long left_shift(const long left, const long right);


/** Returns the \b left shifted right by \b right bits. */
long right_shift(const long left, const long right);


/** Returns the logical or between the two values. */
long or(const long left, const long right);


/** Returns the logical and between the two values. */
long and(const long left, const long right);


/** Returns the logical not of the values. */
long not(const long left);


/** Comparator for any values.
 *
 *  Compares the two given values and returns -1, 0 or 1 depending on whether
 *  the first one is smaller, equal or bigger than the second one.
 *
 *  If the two values are not of the same type, then raises an error.
 *
 *  The function dispatches the call to the correct comparator depending on the
 *  type of the parameters.
 *
 *  WARNING: the values returned by the function are not tagged.
 */
long compare(const long first, const long second);


/** Equality comparison between two values. Calls compare. */
long eq(const long first, const long second);


/** Inequality comparison between two values. Calls compare. */
long neq(const long first, const long second);


/** Less than comparison between two values. Calls compare. */
long lt(const long first, const long second);


/** Less than or equal to comparison between two values. Calls compare. */
long lte(const long first, const long second);


/** Greater than comparison between two values. Calls compare. */
long gt(const long first, const long second);


/** Greater than or equal to comparison between two values. Calls compare. */
long gte(const long first, const long second);

#endif

