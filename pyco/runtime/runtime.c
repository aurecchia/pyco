﻿#include <stdio.h>
#include <stdlib.h>

#include "runtime.h"
#include "tags.h"
#include "primitives.h"
#include "strings.h"
#include "list.h"
#include "closure.h"


void unprintable_value()
{
    fprintf( stderr, "[!] Printing an Unprintable value.");
    exit(-1);
}


void no_input_error()
{
    fprintf( stderr, "[!] No input received.");
    exit(-1);
}


long input()
{
    char * in = (char *) GC_MALLOC( 256 * sizeof(char) );

    if ( (in = fgets(in, 255, stdin)) != NULL)
    {
        char * ptr = NULL;
        long value = strtol(in, &ptr, 10);

        if ( ptr - in == (long) strlen(in) - 1 )
            return tag( value, int_tag );

        in[strlen(in) - 1] = '\0';

        if ( strcmp( in, "True" ) == 0 )
            return tag( 1, bool_tag );

        if ( strcmp( in, "False" ) == 0 )
            return tag( 0, bool_tag );

        if ( strcmp( in, "None" ) == 0 )
            return tag( 0, ref_tag );

        return string_new( in );
    }

    no_input_error();
}


void _dispatch_print(long value)
{
    const int tag = get_tag( value );
    switch ( tag )
    {
        case int_tag:
            int_print( value );
            break;
        case bool_tag:
            bool_print( value );
            break;
        case str_tag:
            string_print( value );
            break;
        case list_tag:
            list_print( value );
            break;
        case ref_tag:
            if ( untag(value) == 0 )
                printf("None");
            break;
        default:
            unprintable_value();
    }
}


void prompt(long value)
{
    _dispatch_print( value );
}


void print(long value)
{
    _dispatch_print( value );
    printf(" ");
}


void print_nl( long value )
{
    _dispatch_print( value );
    printf("\n");
}


void print_empty()
{
    printf("\n");
}


