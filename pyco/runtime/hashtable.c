#include "hashtable.h"
#include <stdlib.h>


unsigned long hash(const char * string, unsigned long mod)
{
  // Because every good hash function has to use 37
  unsigned long hash = 37;

  // Magic happens here
  for (long i = 0; i < strlen(string); ++i)
    hash = ((hash << 5) + hash) + string[i];

  return hash % mod;
}


hashtable_t * new_table(unsigned long size)
{
  hashtable_t * table = GC_MALLOC(sizeof(hashtable_t));
  table->elems = GC_MALLOC(size * sizeof(pair_t *));
  table->size = size;
  return table;
}


pair_t * _new_pair(const char * id, const long value)
{
  // Allocate memory for the structure and the two strings
  pair_t * pair = GC_MALLOC(sizeof(pair_t));
  pair->id = GC_MALLOC(strlen(id) * sizeof(char));
  pair->next = NULL;
  pair->value = value;

  // copy the input strings and return structure
  strcpy(pair->id, id);
  return pair;
}


void hashtable_put(hashtable_t * table, const char * key, const long value)
{
  const long index = hash(key, table->size);

  // If no element at same index
  if (table->elems[index] == NULL)
  {
    table->elems[index] = _new_pair(key, value);
    return;
  }

  // If other elements at same index
  pair_t * elem = table->elems[index];

  // Check with if pair is already in table
  while ( strcmp(key, elem->id) != 0 )
  {
    if ( elem->next == NULL )
    {
      // No more elements, then append pair to beginning
      pair_t * pair = _new_pair(key, value);
      pair->next = table->elems[index];
      table->elems[index] = pair;
      return;
    }

    // There are more elements to check
    elem = elem->next;
  }

  // Update value of already stored pair
  elem->value = value;
}


long hashtable_get(hashtable_t * table, const char * key)
{
  const long index = hash(key, table->size);

  pair_t * elem = table->elems[index];

  // Look for element in table with the same id
  while ( elem != NULL && strcmp(key, elem->id) != 0 )
  {
    if ( elem->next != NULL )
    {
      // There are more elements to check
      elem = elem->next;
    }
    else
    {
      // No more elements, the given key is not in the table
      // 3 represents NULL in our compiler: zeros followed by tag 11
      return 3;
    }
  }

  // Return the value
  return elem->value;
}
