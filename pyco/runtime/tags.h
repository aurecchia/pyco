﻿#ifndef _PYCO_TAGS_H
#define _PYCO_TAGS_H

#define TAG_SIZE 3
#define TAG_MASK 7
#define VALUE_MASK ~1 - 6


/** Enumerator representing the tag of the number. */
typedef enum TAG {
  int_tag,
  bool_tag,
  str_tag,
  ref_tag,
  list_tag
} tag_t;


/** Checks whether the given tag id valid, raises and error if it is not. */
void check_tag(int tag);


/** Returns the name of the tag as a string. */
const char * get_tag_name(int tag);


/** Returns the tag of the value. */
int get_tag(long value);


/** Checks that the value has the given tag. */
int has_tag(long value, tag_t type);


/** Tells whether the given tag is a pointer tag (ref, string, list). */
int is_pointer_tag(int tag);


/** Tells whether the given tag is a primitive tag (integer or boolean). */
int is_primitive_tag(int tag);


/**
 * Tags the value with the given tag (if valid).
 *
 * Before tagging determines whether we are working with a pointer, in that
 * case it does not shift the value before applying the tag.
 */
long tag(long value, tag_t tag);


/** Tags a value AND-ing the tags of two other values (int-bool operations). */
long tag_result(long value, long left, long right);


/** Returns the untagged value. */
long untag(long value);


/** Returns the untagged value and checks that it had the expected tag. */
long untag_check(const long value, const tag_t tag);


/** Returns the value of the given tag (used for comparisons). */
int get_tag_value(const long tag);


#endif // _PYCO_TAGS_H
