﻿#include <gc.h>

#include "closure.h"
#include "tags.h"

int _INITIALIZED = 0;

long make_closure( void * function_pointer )
{
    if ( !_INITIALIZED )
        GC_INIT();

    closure_t * closure = (closure_t *) GC_MALLOC( sizeof(closure_t) );
    closure->function_p = function_pointer;
    closure->env = new_table(32);
    return tag( (long) closure, ref_tag );
}


void put_env( long closure, char * key, long val )
{
    closure_t * closure_ptr = (closure_t *) closure;
    hashtable_put(closure_ptr->env, key, val);
}


long get_env( long closure, char * key )
{
    closure_t * closure_ptr = (closure_t *) closure;
    return hashtable_get(closure_ptr->env, key);
}


