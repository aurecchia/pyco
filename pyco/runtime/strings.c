#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gc.h>

#include "strings.h"
#include "tags.h"


void string_non_primitive_mul_error(const long item)
{
    const char * form = "[!] Multiplying string with non-primitive value: %ld";
    fprintf( stderr, form, untag( item ));
    exit(-1);
}


void index_out_of_range_error()
{
    fprintf( stderr, "[!] String index out of range.");
    exit(-1);
}


void string_non_primitive_index_error()
{
    fprintf( stderr, "[!] String indices must be primitives.");
    exit(-1);
}


void item_not_in_string_error(const long item)
{
    fprintf( stderr, "[!] Item <%ld> not in string.", item);
    exit(-1);
}


long string_new(const void * string)
{
    const int len = strlen( (char *) string ) + 1;

    char * string_ptr = (char *) GC_MALLOC( sizeof(char) * len );
    strcpy( string_ptr, (char *) string );

    return tag ( (long) string_ptr, str_tag );
}


long string_len(const long string)
{
    const char * string_ptr = (char *) untag_check( string, str_tag );
    int len = strlen( string_ptr );
    return tag( len, int_tag );
}


long string_get(const long string, const long index)
{
    const char * string_ptr = (char *) untag_check( string, str_tag );
    const int len = strlen( string_ptr );

    if ( !is_primitive_tag(get_tag(index)) )
        string_non_primitive_index_error();

    int pos = untag( index );
    if ( pos < -len || pos >= len )
        index_out_of_range_error();

    if ( pos < 0 )
        pos += len;

    char * res = (char *) GC_MALLOC( 2 * sizeof( char ) );
    res[0] = string_ptr[pos];
    res[1] = '\0';

    return tag( (long) res, str_tag );
}


long string_index(const long string, const long elem)
{
    const char * string_ptr = (char *) untag_check( string, str_tag );
    const char * elem_ptr = (char *) untag_check( elem, str_tag );

    if ( strlen(elem_ptr) > strlen(string_ptr) )
        item_not_in_string_error(elem);

    const char * pos_ptr = strstr( string_ptr, elem_ptr );

    if ( pos_ptr == NULL )
        item_not_in_string_error(elem);

    const long pos = (long) ( pos_ptr - string_ptr );
    return tag( pos, int_tag );
}


long string_count(const long string, const long elem)
{
    const char * string_ptr = (char *) untag_check( string, str_tag );
    const char * sub = (char *) untag_check( elem, str_tag );

    const int len = strlen( sub );
    if ( len == 0 )
        return tag( strlen(string_ptr) + 1, int_tag );

    const char * from = string_ptr;
    int count = 0;

    for ( from = strstr(from, sub); from; from = strstr(from + len, sub) )
        count++;

    return tag( count, int_tag );
}


long string_slice(const long string, const long from, const long to)
{
    const char * string_ptr = (char *) untag_check( string, str_tag );
    const int len = strlen( string_ptr );

    if ( !is_primitive_tag(get_tag(from)) )
        string_non_primitive_index_error();

    if ( !is_primitive_tag(get_tag(to)) )
        string_non_primitive_index_error();

    int bottom = untag( from );
    if ( bottom < 0 && bottom > -len )
        bottom += len;
    else if ( bottom < -len )
        bottom = 0;
    else if ( bottom > len && len > 0)
        bottom = len;


    int top = untag( to );
    if ( top < 0 && top > -len )
        top += len;
    else if ( top < -len )
        top = 0;
    else if ( top > len && len > 0 )
        top = len;

    int size = top - bottom;
    if ( size < 0 )
        size = 0;

    char * res = (char *) GC_MALLOC( (size + 1) * sizeof( char ) );

    for ( int i = bottom; i < top; ++i )
        res[i - bottom] = string_ptr[i];
    res[size] = '\0';

    return tag( (long) res, str_tag );
}


long string_slice_up_to(const long string, const long to)
{
    const long from = tag( 0, int_tag );

    return string_slice( string, from, to );
}


long string_slice_from(const long string, const long from)
{
    const char * string_ptr = (char *) untag_check( string, str_tag );
    const long to = tag( strlen(string_ptr), int_tag );

    return string_slice( string, from, to );
}


long string_add(const long first, const long second)
{
    const char * first_ptr = (char *) untag_check( first, str_tag );
    const char * second_ptr = (char *) untag_check( second, str_tag );

    const int dest_len = strlen( first_ptr ) + strlen( second_ptr ) + 1;
    char * dest = (char *) GC_MALLOC( dest_len * sizeof(char) );

    strcpy( dest, first_ptr );
    strcat( dest, second_ptr );

    return tag( (long) dest, str_tag );
}


long string_mul(const long string, const long factor)
{
    if ( !is_primitive_tag( get_tag(factor) ) )
        string_non_primitive_mul_error( factor );

    long times = untag( factor );

    if ( times < 0 )
        times = 0;

    char * new_string_ptr = (char *) GC_MALLOC( sizeof(char) );
    new_string_ptr[0] = '\0';

    long new_string = tag( (long) new_string_ptr, str_tag );

    while ( times-- > 0 )
        new_string = string_add( new_string, string );

    return new_string;
}


long string_compare(const long first, const long second)
{
    const char * first_ptr = (char *) untag_check( first, str_tag );
    const char * second_ptr = (char *) untag_check( second, str_tag );

    return strcmp( first_ptr, second_ptr );
}


void string_print(const long string)
{
    const char * string_ptr = (char *) untag_check( string, str_tag );

    printf("%s", string_ptr);
}


