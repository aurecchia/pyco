__author__ = 'auli'

from nodes.AST import *
from pyco.compile import PyCoException


class NotConvertibleError(PyCoException):
    """ Raised when trying to create closures for unsupported objects. """

    def __init__(self, obj, line):
        self.obj = obj
        self.line = line

    def __repr__(self):
        return "Object {} cannot be closure converted!".format(repr(self.obj))


class Converter():

    @staticmethod
    def is_free(node, bound):
        return isinstance(node, Identifier) and node.name not in bound

    @staticmethod
    def get_bound_variables(node):
        bound = set()

        for arg in node.args:
            bound.add(arg.name)

        if isinstance(node, Function):
            for statement in node:
                if isinstance(statement, Assign):
                    for target in statement.targets:
                        bound.add(target.name)

        return bound

    def UnaryOperation(self, unary, bound, env):
        if self.is_free(unary.expr, bound):
            env.vars.append(unary.expr)
            unary.expr = GetEnvVar(unary.expr.name, unary.line)
        else:
            self.convert(unary.expr, bound, env)

    def Binary(self, binary, bound, env):
        if self.is_free(binary.left, bound):
            env.vars.append(binary.left)
            binary.left = GetEnvVar(binary.left.name, binary.line)
        else:
            self.convert(binary.left, bound, env)

        if self.is_free(binary.right, bound):
            env.vars.append(binary.right)
            binary.right = GetEnvVar(binary.right.name, binary.line)
        else:
            self.convert(binary.right, bound, env)

    def Assign(self, assign, bound, env):
        if self.is_free(assign.expr, bound):
            env.vars.append(assign.expr)
            assign.expr = GetEnvVar(assign.expr.name, assign.line)
        else:
            self.convert(assign.expr, bound, env)

    def AugmentedAssign(self, assign, bound, env):
        if self.is_free(assign.expr, bound):
            env.vars.append(assign.expr)
            assign.expr = GetEnvVar(assign.expr.name, assign.line)
        else:
            self.convert(assign.expr, bound, env)

    def If(self, if_s, bound, env):
        if self.is_free(if_s.cond, bound):
            env.vars.append(if_s.cond)
            if_s.cond = GetEnvVar(if_s.cond.name, if_s.line)
        else:
            self.convert(if_s.cond, bound, env)

        for statement in if_s.body:
            self.convert(statement, bound, env)

        for statement in if_s.else_body:
            self.convert(statement, bound, env)

    def While(self, while_s, bound, env):
        if self.is_free(while_s.cond, bound):
            env.vars.append(while_s.cond)
            while_s.cond = GetEnvVar(while_s.cond.name, while_s.line)

        for statement in while_s:
            self.convert(statement, bound, env)

    def FunctionCall(self, call, bound, env):
        args = []

        if self.is_free(call.name, bound):
            env.vars.append(call.name)
            call.name = GetEnvVar(call.name.name, call.line)

        for arg in call.args:
            if self.is_free(arg, bound):
                env.vars.append(arg)
                args.append(GetEnvVar(arg.name, call.line))
            else:
                args.append(arg)
        call.args = args

    def Function(self, function, bound, env):
        args = []

        for arg in function.args:
            if self.is_free(arg, bound):
                env.vars.append(arg)
                args.append(GetEnvVar(arg.name, function.line))
            else:
                args.append(arg)
        function.args = args

    def Print(self, print_s, bound, env):
        args = []

        for arg in print_s.args:
            if self.is_free(arg, bound):
                env.vars.append(arg)
                args.append(GetEnvVar(arg.name, print_s.line))
            else:
                args.append(arg)
        print_s.args = args

    def Input(self, input_f, bound, env):
        args = []

        for arg in input_f.args:
            if self.is_free(arg, bound):
                env.vars.append(arg)
                args.append(GetEnvVar(arg.name, input_f.line))
            else:
                args.append(arg)
        input_f.args = args

    def Return(self, ret, bound, env):
        return self.convert(ret.value, bound, env)

    def Discard(self, discard, bound, env):
        return self.convert(discard.expr, bound, env)

    def convert(self, node, bound, env):
        if hasattr(self, node.__class__.__name__):
            return getattr(self, node.__class__.__name__)(node, bound, env)


def get_environment(function):
    converter = Converter()
    bound = converter.get_bound_variables(function)
    env = MakeEnv(function.line)

    if isinstance(function, Function):
        statements = function.code
    else:
        statements = [function.code]

    for statement in statements:
        converter.convert(statement, bound, env)

    return env





