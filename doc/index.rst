.. PyCo documentation master file, created by
   sphinx-quickstart on Wed Oct  2 01:21:41 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyCo's documentation!
###################################


PyCo is a Compiler from Python to LLVM, written in Python.

Credits
*******
* Argenti Marco
* Aurecchia Alessio
* Azara Antonio

********

.. toctree::
   :maxdepth: 2

   readme
   doc

.. autosummary::
    
    PyCo

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

