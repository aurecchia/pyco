def f(x):
    if x == 2:
        return 2
    elif x == 3:
        return 3
    else:
        return 6


def g(x, y):
    if x == y:
        return 0
    elif x > y:
        return 1
    else:
        return -1


def h(x, y):
    print 5
    if x % 2 == 0:
        return y
    else:
        return 0

a = f(2)
print a

a = f(3)
print a

a = f(7)
print a

a = g(2, 2)
print a

a = g(3, 2)
print a

a = g(1, 2)
print a

a = h(2, 6)
print a

a = h(7, 6)
print a
