
a = [1, 3, 4, 5]
b = [4, 5, 7, 8]

print a

print a and None
print None and a

print a or None
print None or a

print not a

print a and b
print b and a

print a or b
print b or a

print a or False
print a or True

print True or a
print False or a

print a and True
print a and False

print True and a
print False and a

