#!/usr/bin/env python


x = True or 5
print x


y = 5 or False
print y



if 3 < 10:
    print 3 < 10
else:
    print 3 >= 10



if 4 > 1:
    print 4
else:
    print 1



eq = 1
if eq == 1:
    print eq
else:
    print 0

if 1 <= 2:
    print 3


if eq >= 5:
    print 5
elif eq > 0:
    print 1
else:
    print -1



if False:
    print False
else:
    print True



if 3 or 2:
    print 3
else:
    print 2



if False or False:
    print False
elif False or True:
    print True
else:
    print False



z = False and 2
print z



z = 3 and True
print z
