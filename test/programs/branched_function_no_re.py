def f(x):
    if x == 2:
        print 2
    elif x == 3:
        print 3
    else:
        print 6


def g(x, y):
    if x == y:
        print 0
    elif x > y:
        print 1
    else:
        print -1


def h(x, y):        
    print 5
    if x % 2 == 0:
        print y
    else:
        print 0

f(2)
f(3)
f(7)

g(2, 2)
g(3, 2)
g(1, 2)

h(2, 6)
h(7, 6)
