if True:
    print 3

if False:
    print 3

if False:
    a = 3
    print a
elif True:
    a = 4
    print a

if False:
    print 3
elif False:
    print 6
else:
    print 12

if True:
    print 3
elif True:
    print 6

if True:
    print 3
elif True:
    print 6
else:
    print 12

if False:
    print 33
elif False:
    print 6
elif False:
    print 6
elif False:
    print 6
elif False:
    print 6

if 1:
    print 3

if 0:
    print 3

if 0:
    a = 3
    print a
elif 1:
    a = 4
    print a

if 0:
    print 3
elif 0:
    print 6
else:
    print 12

if 1:
    print 3
elif 1:
    print 6

if 1:
    print 3
elif 1:
    print 6
else:
    print 12

if 0:
    print 33
elif 0:
    print 6
elif 0:
    print 6
elif 0:
    print 6
elif 0:
    print 6
x=1
if True:
    if x==1:
        print 1
    else:
        print 2
else:
    print 3

x=1
if True:
    if x==3:
        print 4
else:
    print 5


if True:
    if False:
        print 6
    else:
        print 7
