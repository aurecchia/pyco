# to workaround Jacques group's compiler bug
tt = True
ff = False

def cons(a, b):
    def get(d):
        if d:
            return a
        else:
            return b
    return get

def car(p):
    return p(tt)

def cdr(p):
    return p(ff)

def fib(n):
    def helper(n):
        if n == 0:
            return cons(1, 1)
        if n == 1:
            return cons(1, 2)
        p = helper(n/2 - 1)
        a = car(p)
        b = cdr(p)
        c = a + b
        if n % 2 == 0:
            return cons(a*a + b*b, c*c - a*a)
        return cons(c*c - a*a, b*b + c*c)
    # to workaround a bug in Alessio group's compiler
    tmp = helper(n)
    return car(tmp)

i = 0
while i < 15:
    print fib(i)
    i += 1
