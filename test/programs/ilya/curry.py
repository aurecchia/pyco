
def curry(f):
    return lambda x : lambda y : f(x, y)

def uncurry(f):
    return lambda x, y : f(x)(y)

def add(x,y):
    return x+y

f = curry(add)(3)

print f(4)

g = uncurry(curry(add))

print g(34, 1)
