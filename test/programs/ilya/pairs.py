
def makePair(a, b):
    def get(t):
        if t:
            return a
        else:
            return b
    return get

p = makePair(1, True)
print p(True)
print p(False)

