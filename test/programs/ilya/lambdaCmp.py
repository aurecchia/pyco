
print (lambda x : x + 1) == (lambda x : x + 1)
print (lambda x : x * x) == (lambda x : x + 1)

def f(x):
    return x + 1

def g(x):
    return x + 1

print f == (lambda x : x + 1)
print g == f
print f == f
