
t = True
f = False

def makePair(a, b):
    def get(t):
        if t:
            return a
        else:
            return b
    return get

def fibH(n):
    if n == 0:
        return makePair(0, 1)
    x = fibH(n - 1)
    return makePair(x(f), x(t) + x(f))

def fib(n):
    tmp = fibH(n)
    return tmp(f)

n = 0
while n < 15:
    print fib(n)
    n += 1

