#include "../../../pyco/runtime/hashtable.h"
#include <time.h>
#include <stdlib.h>

int errors = 0;

static const char alphanum[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";


/**
 * @brief Generates a alphanumeric string of the given length.
 *
 * @param int The length of the string to generate.
 * @return The random string.
 */
const char * rand_str(const unsigned int len) {

  char * str = (char *) malloc(len * sizeof(char));

  for (unsigned int i = 0; i < len; ++i) {
   str[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  }

  str[len] = 0;
  return str;
}


/**
 * @brief Checks the result of the retrieval and reports the error.
 *
 * @param key The name of the test.
 * @param expected The expected value.
 * @param actual The actual value.
 */
void check_res(const char * key, const int expected, const int actual)
{
  if (expected != actual)
  {
    printf(">>> %s\n", key);
    printf("expected: %d, actual: %d\n\n", expected, actual);
    errors++;
  }
}


/**
 * @brief Tests a simple retrieval in the hash table.
 */
void simple_retrieval()
{
  hashtable_t * table = new_table(256);

  hashtable_put(table, "simple_retrieval", 10);
  check_res("simple_retrieval", 10, hashtable_get(table, "simple_retrieval"));
}


/**
 * @brief Checks a simple update to an already stored value.
 */
void simple_update()
{
  hashtable_t * table = new_table(256);

  hashtable_put(table, "simple_update", 10);
  hashtable_put(table, "simple_update", 20);
  check_res("simple_update", 20, hashtable_get(table, "simple_update"));
}


/**
 * @brief Tests multiple updates to stored values.
 */
void multiple_update()
{
  hashtable_t * table = new_table(256);

  for (int i = 0; i < 40; ++i)
  {
    hashtable_put(table, "multiple_update", i);
  }
  hashtable_put(table, "multiple_update", 70);
  check_res("multiple_update", 70, hashtable_get(table, "multiple_update"));
}


/**
 * @brief Tests the behavior of the hash table with thousands of values.
 */
void thousand_values()
{
  const char * keys[10000] = {};
  int values[10000] = {};


  hashtable_t * table = new_table(256);

  for (int i = 0; i < 10000; ++i)
  {
    const char * str = rand_str(10);
    const int val = rand();
    hashtable_put(table, str, val);

    keys[i] = str;
    values[i] = val;
  }

  for (int i = 0; i < 10000; ++i)
    check_res("thousand_values", values[i], hashtable_get(table, keys[i]));
}



int main(void)
{
  srand(time(NULL));

  simple_retrieval();
  simple_update();
  multiple_update();
  thousand_values();

  if (errors)
  {
    printf("There were %d failure/s in the hashtable test!\n", errors);
    return 1;
  }
  printf("There were no failures in the hashtable test!\n");
  return 0;
}
