#!/bin/bash

PKG="pyco"
OLD_PATH="$PWD"
TESTS_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#Go to the test directory
cd $( dirname "${BASH_SOURCE[0]}" )
cd ../..

FAILURES=0
ERRORS=0
PASSED=0
TOTAL=0


mkdir -p compiled
mkdir -p errors

RUNTIME_DIR="$PKG/runtime/"
LIBS=`ls $RUNTIME_DIR*.c`

printf "Generating runtime libs\n"

for LIB in $LIBS
do
    echo ">>> $LIB"
    gcc -c -std=c99 $LIB -o ${LIB%.*}.o
done

gcc -std=c99 $TESTS_PATH/hashtable/test.c $RUNTIME_DIR/hashtable.o -lgc -o compiled/hash_test
OUT_HASH=`compiled/hash_test`
RET_HASH=$?
if [[ $RET_HASH != 0 ]];
then
    echo "[!] ERROR: some tests for the hash table failed!"
    echo $OUT_HASH
else
    echo "    Tests for hash table passed."
fi

if [[ $1 ]];
then
  if [[ $1 == *.py ]];
  then
    TESTS=$1
  else
    TESTS=$TESTS_PATH/$1.py
  fi
else
  TESTS=`ls $TESTS_PATH/*.py`
  TESTS=$TESTS+`ls $TESTS_PATH/ilya/*.py`
fi

TESTSUITE_START=$(python -c'import time; print repr(time.time())')
for TEST_FILE in $TESTS
do
    TEST_START=$(python -c'import time; print repr(time.time())')
    TEST=$(basename $TEST_FILE)
    TEST=${TEST%.*}
    TOTAL=$((TOTAL+1))
    NEW_LINE=false # For formatting
    printf "\n"
    printf "==== Running $TEST...\n"
    TEST_XML=""

    OUT_PY=`yes 42 2>/dev/null | python $TEST_FILE 2> errors/$TEST.ref.log`
    RET_PY=$?

    OUT_COMPILE=`coverage run --append --branch --source=$PKG $PKG/compile.py $TEST_FILE \
            2> errors/$TEST.act.log > compiled/$TEST.ll`
    RET_COMPILE=$?

    ERRORS_REF=$(<errors/$TEST.ref.log)

    if [[ ( $RET_COMPILE == 0 ) ]];
    then
        OUT_LLC=`llc compiled/$TEST.ll -filetype=obj -o compiled/$TEST.o 2>> errors/$TEST.act.log`
        RET_LLC=$?
    else
        OUT_LLC="[!] ERROR: llc could not generate ${TEST}.ll"
        RET_LLC=1
    fi

    if [[ ( $RET_LLC == 0 ) ]];
    then
        LIBS=`ls $RUNTIME_DIR*.o`
        OUT_LINK=`gcc compiled/$TEST.o $LIBS -o compiled/$TEST -lgc -lm 2>> errors/$TEST.act.log`
        RET_LINK=$?
    else
        OUT_LINK="[!] ERROR: gcc could not generate executable $TEST"
        RET_LINK=1
    fi

    if [[ ( $RET_LLC == 0 ) ]];
    then
        OUT_RUN=`yes 42 2>/dev/null | compiled/$TEST 2>> errors/$TEST.act.log`
        RET_RUN=$?
    else
        OUT_RUN="[!] ERROR: The executable $TEST could not be run."
        RET_RUN=1
    fi

    OUTPUT=`echo "$OUT_COMPILE\n$OUT_LLC\n$OUT_LINK\nOUT_RUN"`

    ERRORS_ACTUAL=$(<errors/$TEST.act.log)

    if [[ ( $RET_PY > 0 && $RET_RUN > 0 ) ]] || \
       [[ $TEST == warn_* && ! -z $ERRORS_ACTUAL && \
              $ERRORS_ACTUAL == WARNING* && $RET_PY == 0 && \
              $RET_RUN == 0 ]] || \
       [[ ( "$OUT_PY" == "$OUT_RUN" && $RET_PY == 0 && $RET_RUN == 0 ) ]] || \
       [[ ( $RET_PY == 0 && $RET_RUN > 0 && $TEST == fail_* ) ]];
    then
        PASSED=$((PASSED+1))
        if [[ ( $RET_RUN == 1) ]] || [[ $TEST == warn_* ]];
        then
        echo ""
            echo ">>>> Expected output:"
            if [[ ${#OUTPUT} -gt 3 ]]; then
                    echo "     (No output)"
            else
                    echo -e "     $OUTPUT"
            fi
            echo ">>>> Expected errors:"
            if [[ -z $ERRORS_ACTUAL ]]; then
                    echo "     (No errors)"
            else
                    echo "     $ERRORS_ACTUAL"
            fi

            printf "\n>>>> Test $TEST was supposed to fail\n  -> PASS\n"
        else
            printf "  -> PASS\n"
        fi
				TEST_END=$(python -c'import time; print repr(time.time())')
				TEST_TIME=$(echo "$TEST_END - $TEST_START" | bc)
				printf "(${TEST_TIME}s)\n"
    else
        NEW_LINE=true
        if [[ $ERRORS_ACTUAL == *"Traceback (most recent call last):"* ]]
        then
            ERRORS=$((ERRORS+1))
            echo "  -> ERRORED"
        elif [[ $ERRORS_ACTUAL == "lli:"* || $ERRORS_ACTUAL == *"\nlli:"* ]]
        then
            ERRORS=$((ERRORS+1))
            echo "  -> ERRORED (Compiled file is incorrect.)"
        else
            FAILURES=$((FAILURES+1))
            printf "  -> FAILED"
        fi
				TEST_END=$(python -c'import time; print repr(time.time())')
				TEST_TIME=$(echo "$TEST_END - $TEST_START" | bc)
				printf "(${TEST_TIME}s)\n"

				printf "\n"
				echo ">>>> Reference output:"
        echo $OUT_PY
        echo ">>>> Reference errors:"
        echo $ERRORS_REF
        echo ">>>> Actual output:"
        echo $OUT_RUN
        echo ">>>> Actual errors:"
        echo $ERRORS_ACTUAL
    fi
done
printf "\n==== Results:"
echo "$PASSED test of $TOTAL passed"

TESTSUITE_END=$(python -c'import time; print repr(time.time())')
TESTSUITE_TIME=$(echo "$TESTSUITE_END - $TESTSUITE_START" | bc)
echo "Duration: ${TESTSUITE_TIME} seconds."

cd "$OLD_PATH"

if [[ $PASSED == $TOTAL ]]
then
    echo "TESTS: SUCCESS"
else
    echo "TESTS: FAILURE"
fi

exit 0
