if True:
    if True:
        if True:
            print 3
    elif True:
        print 7

if False:
    print 3
elif True:
    if False:
        print 3
    elif True:
        print 7
else:
    print 3

if False:
    print 3
elif False:
    print 3
else:
    if False:
        print 3
    elif False:
        print 3
    else:
        print 7

if 1:
    if 1:
        if 1:
            print 3
    elif 1:
        print 7

if 0:
    print 3
elif 1:
    if 0:
        print 3
    elif 1:
        print 7
else:
    print 3

if 0:
    print 3
elif 0:
    print 3
else:
    if 0:
        print 3
    elif 0:
        print 3
    else:
        print 7
